# Intelligenza Artificiale

Riassunto del corso di intelligenza artificiale.

Fonti:

- Slide [Prof. Sperduti](https://www.math.unipd.it/~sperduti), [Prof. Ballan](https://www.math.unipd.it/~ballan)
- S. Russell, P. Norvig, “Artificial Intelligence: a Modern Approach”, Prentice Hall, 2010, III edizione.

Autore: Filippo Berto

[Versione PDF](intelligenza-artificiale.pdf)

## Indice

1. [Indice](#Indice)
1. [Teoria degli agenti](#Teoria-degli-agenti)
   1. [Agente](#Agente)
   1. [Astrazione matematica](#Astrazione-matematica)
      1. [Esempio del robot aspirapolvere](#Esempio-del-robot-aspirapolvere)
   1. [Razionalità](#Razionalità)
   1. [Performance Environment Actuators Sensors (PEAS)](#Performance-Environment-Actuators-Sensors-PEAS)
      1. [Performance](#Performance)
      1. [Ambiente](#Ambiente)
      1. [Attuatori](#Attuatori)
      1. [Sensori](#Sensori)
   1. [Tipi di agente](#Tipi-di-agente)
      1. [A riflesso semplice](#A-riflesso-semplice)
         1. [Problema dell'agente a riflesso semplice](#Problema-dellagente-a-riflesso-semplice)
         1. [Esempio agente a riflesso semplice](#Esempio-agente-a-riflesso-semplice)
      1. [A riflesso con stato](#A-riflesso-con-stato)
      1. [Basati su goal](#Basati-su-goal)
      1. [Basati su misura di utilità](#Basati-su-misura-di-utilità)
   1. [Agenti che apprendono](#Agenti-che-apprendono)
1. [Ricerca non informata](#Ricerca-non-informata)
1. [Risoluzione dei problemi](#Risoluzione-dei-problemi)
   1. [Astrazione dello spazio degli stati per semplificare il problema](#Astrazione-dello-spazio-degli-stati-per-semplificare-il-problema)
1. [Ricerca su grafi e alberi](#Ricerca-su-grafi-e-alberi)
   1. [Differenza tra nodo e stato](#Differenza-tra-nodo-e-stato)
   1. [Ricerca su alberi](#Ricerca-su-alberi)
   1. [Ricerca su grafi](#Ricerca-su-grafi)
1. [Algoritmi di ricerca](#Algoritmi-di-ricerca)
   1. [Breadth First Search (BFS) - Ricerca in ampiezza](#Breadth-First-Search-BFS---Ricerca-in-ampiezza)
      1. [Proprietà BFS](#Proprietà-BFS)
   1. [Depth First Search (DFS) - Ricerca in profondità](#Depth-First-Search-DFS---Ricerca-in-profondità)
      1. [Proprietà DFS](#Proprietà-DFS)
   1. [Iterative Depth first Search (IDS) - Ricerca in profondità iterativa](#Iterative-Depth-first-Search-IDS---Ricerca-in-profondità-iterativa)
      1. [Proprietà IDS](#Proprietà-IDS)
   1. [Bidirectional Search (BS)](#Bidirectional-Search-BS)
   1. [Tabella riassuntiva](#Tabella-riassuntiva)
1. [Ricerca informata](#Ricerca-informata)
   1. [Ricerca Best first](#Ricerca-Best-first)
   1. [Ricerca greedy - Best First](#Ricerca-greedy---Best-First)
      1. [Esempio di funzione euristica](#Esempio-di-funzione-euristica)
      1. [Proprietà della ricerca greedy](#Proprietà-della-ricerca-greedy)
   1. [Ricerca A*](#Ricerca-A)
      1. [Esempio di euristica ammissibile](#Esempio-di-euristica-ammissibile)
      1. [Ottimalità A* sugli alberi](#Ottimalità-A-sugli-alberi)
         1. [Lemma sull'espansione di A*](#Lemma-sullespansione-di-A)
      1. [Ottimalità A* sui grafi](#Ottimalità-A-sui-grafi)
      1. [Proprietà di A*](#Proprietà-di-A)
      1. [Ottimalità di A*](#Ottimalità-di-A)
   1. [Iterative Deepening A* (IDA*)](#Iterative-Deepening-A-IDA)
   1. [Recursive Best First Search (RBFS)](#Recursive-Best-First-Search-RBFS)
      1. [Proprietà di RBFS](#Proprietà-di-RBFS)
   1. [Simplified Memory-bounded A* (SMA*)](#Simplified-Memory-bounded-A-SMA)
      1. [Proprietà di SMA*](#Proprietà-di-SMA)
1. [Costruzione sistematica di euristiche](#Costruzione-sistematica-di-euristiche)
   1. [Esempio di costruzione sistematica di un euristica](#Esempio-di-costruzione-sistematica-di-un-euristica)
1. [Algoritmi di miglioramento iterativo](#Algoritmi-di-miglioramento-iterativo)
   1. [Esempio del commesso viaggiatore](#Esempio-del-commesso-viaggiatore)
   1. [Hill-climbing](#Hill-climbing)
      1. [Esempio delle n-regine](#Esempio-delle-n-regine)
   1. [Problemi di Hill-climbing](#Problemi-di-Hill-climbing)
      1. [Massimi locali](#Massimi-locali)
      1. [Plateau](#Plateau)
      1. [Simulazione pratica del problema delle 8 regine](#Simulazione-pratica-del-problema-delle-8-regine)
         1. [Hill-climbing standard](#Hill-climbing-standard)
         1. [Hill-climbing con mosse laterali (non più di 100 consecutive)](#Hill-climbing-con-mosse-laterali-non-più-di-100-consecutive)
         1. [Hill-climbing con riavvio casuale](#Hill-climbing-con-riavvio-casuale)
   1. [Simulated annealing](#Simulated-annealing)
   1. [Discesa di gradiente](#Discesa-di-gradiente)
1. [Ricerca online](#Ricerca-online)
   1. [Problemi della ricerca online](#Problemi-della-ricerca-online)
   1. [Ricerca online tramite DFS](#Ricerca-online-tramite-DFS)
   1. [Ricerca casuale](#Ricerca-casuale)
   1. [Ricerca Learning Real-Time A* (LRTA*)](#Ricerca-Learning-Real-Time-A-LRTA)
1. [Algoritmi per giochi](#Algoritmi-per-giochi)
   1. [Tipi di giochi](#Tipi-di-giochi)
   1. [Minmax](#Minmax)
   1. [$\alpha$-$\beta$ pruning](#alpha-beta-pruning)
      1. [Analisi del best case con ordine perfetto](#Analisi-del-best-case-con-ordine-perfetto)
   1. [Limiti alle risorse](#Limiti-alle-risorse)
      1. [Esempio di funzione di valutazione](#Esempio-di-funzione-di-valutazione)
1. [Giochi non deterministici](#Giochi-non-deterministici)
1. [Giochi ad informazione parziale](#Giochi-ad-informazione-parziale)
   1. [Esempio gioco ad informazione parziale: Bridge](#Esempio-gioco-ad-informazione-parziale-Bridge)
   1. [Analisi corretta](#Analisi-corretta)
1. [Agenti basati su goal](#Agenti-basati-su-goal)
   1. [Modelli](#Modelli)
      1. [Esempio di inferenza](#Esempio-di-inferenza)
      1. [Esempio di modello: il mondo dei Wumpus](#Esempio-di-modello-il-mondo-dei-Wumpus)
         1. [Ambiente del mondo dei Wumpus](#Ambiente-del-mondo-dei-Wumpus)
         1. [Mistura di prestazione del mondo dei Wumpus](#Mistura-di-prestazione-del-mondo-dei-Wumpus)
         1. [Sensori del mondo dei Wumpus](#Sensori-del-mondo-dei-Wumpus)
         1. [Attuatori del mondo dei Wumpus](#Attuatori-del-mondo-dei-Wumpus)
         1. [Esempio pratico mondo dei Wumpus](#Esempio-pratico-mondo-dei-Wumpus)
   1. [Inferenza per mezzo di enumerazione](#Inferenza-per-mezzo-di-enumerazione)
   1. [Metodi di prova](#Metodi-di-prova)
   1. [Forward e backward chaining](#Forward-e-backward-chaining)
      1. [Forma di Horn ristretta](#Forma-di-Horn-ristretta)
         1. [Esempi di forme di Horn ristrette](#Esempi-di-forme-di-Horn-ristrette)
      1. [Modus Ponens per la forma di Horn](#Modus-Ponens-per-la-forma-di-Horn)
      1. [Forward Chaining](#Forward-Chaining)
         1. [Completezza Forward Chaining](#Completezza-Forward-Chaining)
      1. [Backward chaining](#Backward-chaining)
      1. [Confronto tra FC e BC](#Confronto-tra-FC-e-BC)
   1. [Forma Normale Congiuntiva (CNF universale)](#Forma-Normale-Congiuntiva-CNF-universale)
      1. [Esempi di Forme Normali Congiuntive](#Esempi-di-Forme-Normali-Congiuntive)
      1. [Risoluzione della regola di inferenza per CNF](#Risoluzione-della-regola-di-inferenza-per-CNF)
      1. [Conversione in CNF](#Conversione-in-CNF)
      1. [Algoritmo di risoluzione per CNF](#Algoritmo-di-risoluzione-per-CNF)
         1. [Completezza della risoluzione](#Completezza-della-risoluzione)
1. [Logica del Primo Ordine](#Logica-del-Primo-Ordine)
   1. [Verità nella Logica del Primo Ordine](#Verità-nella-Logica-del-Primo-Ordine)
   1. [Quantificatori Universale ed Esistenziale](#Quantificatori-Universale-ed-Esistenziale)
   1. [Proprietà dei quantificatori](#Proprietà-dei-quantificatori)
   1. [Inferenza: Instanziazione Universale (UI)](#Inferenza-Instanziazione-Universale-UI)
   1. [Inferenza: Instanziazione Esistenziale (EI)](#Inferenza-Instanziazione-Esistenziale-EI)
   1. [Inferenza: applicazione delle istanziazioni](#Inferenza-applicazione-delle-istanziazioni)
   1. [Riduzione alla'inferenza proposizionale](#Riduzione-allainferenza-proposizionale)
   1. [Riduzione](#Riduzione)
   1. [Problemi con la proposizionalizzazione](#Problemi-con-la-proposizionalizzazione)
   1. [Unificazione](#Unificazione)
   1. [Modus Ponens Generalizzato](#Modus-Ponens-Generalizzato)
   1. [Algoritmo di Forward Chaining per FOL](#Algoritmo-di-Forward-Chaining-per-FOL)
      1. [Proprietà del Forward Chaining per FOL](#Proprietà-del-Forward-Chaining-per-FOL)
      1. [Esempio di KB del Forward Chaining](#Esempio-di-KB-del-Forward-Chaining)
      1. [Efficienza del Forward Chaining](#Efficienza-del-Forward-Chaining)
   1. [Algoritmo di Backward Chaining per FOL](#Algoritmo-di-Backward-Chaining-per-FOL)
      1. [Esempio di KB del Backward Chaining](#Esempio-di-KB-del-Backward-Chaining)
      1. [Proprietà del Backward Chaining per FOL](#Proprietà-del-Backward-Chaining-per-FOL)
   1. [Conversione a CNF](#Conversione-a-CNF)
   1. [Strategie di risoluzione](#Strategie-di-risoluzione)
   1. [Schema prova di completezza](#Schema-prova-di-completezza)
1. [Incertezza](#Incertezza)
   1. [Probabilità](#Probabilità)
      1. [Probabilità Soggettiva o Bayesiana](#Probabilità-Soggettiva-o-Bayesiana)
      1. [Decidere nell'incertezza](#Decidere-nellincertezza)
   1. [Probabilità a priori](#Probabilità-a-priori)
   1. [Probabilità condizionale](#Probabilità-condizionale)
      1. [Definizione di Probabilità condizionale](#Definizione-di-Probabilità-condizionale)
   1. [Inferenza tramite enumerazione](#Inferenza-tramite-enumerazione)
   1. [Indipendenza](#Indipendenza)
   1. [Indipendenza condizionale](#Indipendenza-condizionale)
   1. [Regola di Bayes](#Regola-di-Bayes)
      1. [Regola di Bayes e indipendenza condizionale](#Regola-di-Bayes-e-indipendenza-condizionale)
      1. [Esempio probabilistico del mondo de Wumpus](#Esempio-probabilistico-del-mondo-de-Wumpus)
1. [Reti Bayesiane (Bayesian Networks)](#Reti-Bayesiane-Bayesian-Networks)
   1. [Compattezza](#Compattezza)
   1. [Semantica globale](#Semantica-globale)
   1. [Complessità dell'inferenza esatta](#Complessità-dellinferenza-esatta)
   1. [Inferenza tramite simulazione stocastica](#Inferenza-tramite-simulazione-stocastica)
1. [Apprendimento automatico](#Apprendimento-automatico)
   1. [Task](#Task)
   1. [Misura di prestazione](#Misura-di-prestazione)
   1. [Esperienza](#Esperienza)
   1. [Principali paradigmi di apprendimento](#Principali-paradigmi-di-apprendimento)
      1. [Apprendimento Supervisionato](#Apprendimento-Supervisionato)
      1. [Apprendimento Non Supervisionato](#Apprendimento-Non-Supervisionato)
      1. [Apprendimento Con Rinforzo](#Apprendimento-Con-Rinforzo)
   1. [Spazio delle ipotesi](#Spazio-delle-ipotesi)
   1. [Perceptron](#Perceptron)
      1. [Limiti del Perceptron](#Limiti-del-Perceptron)
   1. [Errore Ideale](#Errore-Ideale)
   1. [Errore Empirico](#Errore-Empirico)
1. [Apprendimento Con rinforzo](#Apprendimento-Con-rinforzo)
1. [Processi di Decisione di Markov](#Processi-di-Decisione-di-Markov)
   1. [La Funzione di Valutazione](#La-Funzione-di-Valutazione)
   1. [La funzione $Q()$](#La-funzione-Q)
   1. [Come apprendere $Q()$](#Come-apprendere-Q)
   1. [Convergenza di Q-learning](#Convergenza-di-Q-learning)
1. [Natural Language Processing](#Natural-Language-Processing)
   1. [Knowledge acquisition](#Knowledge-acquisition)
   1. [Modello $n$-gram](#Modello-n-gram)
      1. [Usi del modello $n$-gram](#Usi-del-modello-n-gram)
      1. [Problemi del modello $n$-gram](#Problemi-del-modello-n-gram)
      1. [Lisciare i modelli $n$-gram](#Lisciare-i-modelli-n-gram)
      1. [Valutazione del modello $n$-gram](#Valutazione-del-modello-n-gram)
      1. [$n$-gram e chain rule](#n-gram-e-chain-rule)
   1. [Classificazione del testo](#Classificazione-del-testo)
   1. [Bag of words](#Bag-of-words)
   1. [Parole come simboli discreti](#Parole-come-simboli-discreti)
   1. [Rappresentare parole tramite il loro contesto](#Rappresentare-parole-tramite-il-loro-contesto)
      1. [Vector space model](#Vector-space-model)
   1. [Information Retrieval](#Information-Retrieval)
      1. [TD-IDF weighting](#TD-IDF-weighting)
      1. [Valutazione del modello](#Valutazione-del-modello)
      1. [Question Answering](#Question-Answering)
   1. [Information Extraction](#Information-Extraction)
   1. [Agenti basati su Information Retrieval](#Agenti-basati-su-Information-Retrieval)
      1. [Agenti Frame-Based](#Agenti-Frame-Based)
1. [Computer Vision](#Computer-Vision)
   1. [Sistemi di camere](#Sistemi-di-camere)
   1. [Image Formation](#Image-Formation)
      1. [Notazione del sistema di assi](#Notazione-del-sistema-di-assi)
      1. [Punto di fuga (Vanishing point)](#Punto-di-fuga-Vanishing-point)
      1. [Svantaggi della camera oscura](#Svantaggi-della-camera-oscura)
      1. [Dalla camera oscura alla camera a lenti](#Dalla-camera-oscura-alla-camera-a-lenti)
   1. [Visione Binoculare](#Visione-Binoculare)
      1. [Geometria Epipolare](#Geometria-Epipolare)
      1. [Stereo matching](#Stereo-matching)
         1. [Basic Stereo Matching](#Basic-Stereo-Matching)
      1. [Il problema della corrispondenza](#Il-problema-della-corrispondenza)
   1. [Informazione semantica](#Informazione-semantica)
      1. [Classificazione delle immagini](#Classificazione-delle-immagini)
         1. [Fully Connected Layer](#Fully-Connected-Layer)
         1. [Convolution Layer](#Convolution-Layer)
         1. [Pooling Layer](#Pooling-Layer)
      1. [Funzioni di attivazione](#Funzioni-di-attivazione)

## Teoria degli agenti

### Agente

Un agente è un'entità che percepisce ed agisce.
Cerca di ottenere l'obiettivo data l'informazione disponibile o che può acquisire con le sue azioni.

### Astrazione matematica

Un agente può essere astratto come una funzione da tutte le possibili percezioni $\mathcal{P}^*$ ad azioni ammissibili $\mathcal{A}$:
$$
f:\mathcal{P}^*\rightarrow\mathcal{A}
$$
Tale funzione è detta funzione agente.
Il programma di un agente viene eseguito sulla sua architettura fisica e attua la funzione $f$.

#### Esempio del robot aspirapolvere

Spazio delle percezioni formato dalle coppie $\langle Posizione,Pulito\rangle$ con $Posizione\equiv$ stanza in cui si trova e $Pulito\equiv$ stato di pulizia della stanza.

Lo spazio delle azioni: $\lbrack Sinistra,Destra,Aspira,NoOp\rbrack$, che includono il movimento, l'azione di pulizia è l'azione che non fa nulla.

### Razionalità

Viene fissata una misura di prestazione che valuta la sequenza di percezioni.

**Esempio**:

- $+1$ per ogni spazio pulito in tempo $T$
- $+1$ per ogni spazio pulito per istante di tempo, $-1$ per spostamento
- ...

Un agente razionale sceglie una qualunque azione che massimizza il valore aspettato della misura di prestazione, data la sequenza di percezioni ottenuta fino all'istante corrente.

La razionalità non è né onniscenza, né chiaroveggenza.
Inoltre non è necessariamente definibile con il successo nel raggiungere un obiettivo.

Definiamo la razionalità come la capacità di un agente di esplorare, apprendere ed essere autonomo nelle proprie scelte.

### Performance Environment Actuators Sensors (PEAS)

Per progettare un agente razionale è necessario definirne l'ambiente operativo all'interno del quale dovrà svolgere il suo compito.

Separiamo la definizione in quattro campi:

#### Performance

Come misurare le prestazioni dell'agente.
Misuriamo la sicurezza, il raggiungimento di una destinazione, il profitto ottenuto?

#### Ambiente

In che tipo di ambiente l'agente dovrà operare.

- Osservabile
- Deterministico
- Episodico
- Statico
- Discreto/Analogico
- Agente singolo/Agenti multipli

#### Attuatori

Quali dispositivi di attuazione sono controllati dall'agente.
Anche in base a questo viene definito lo spazio delle azioni possibili.

#### Sensori

Quali dispositivi di percezione sono controllati dall'agente.
Anche in base a questo viene definito lo spazio delle possibili percezioni.

### Tipi di agente

Definiamo in generale i seguenti quattro tipi di agenti.
Questi tipi di agente possono essere trasformati in agenti che apprendono.

#### A riflesso semplice

![Agente a riflesso semplice](immagini/agente-a-riflesso-semplice.png)

- I sensori percepiscono lo stato attuale del mondo;
- L'agente sceglie cosa fare in base a delle regole condizione-azione.

##### Problema dell'agente a riflesso semplice

Se l'ambiente è parzialmente osservabile può fallire, poiché potrebbero non esserci abbastanza informazioni per decidere correttamente la prossima azione.
Una soluzione parziale è quella di aggiungere un elemento di randomizzazione, che permette di sbloccare le situazioni di stallo, questo però necessita di mantenere traccia delle percezioni passate, in uno stato interno.

##### Esempio agente a riflesso semplice

Macchinina radiocomandata che continua a spingere contro un muro

#### A riflesso con stato

![Agente a riflesso con stato](immagini/agente-a-riflesso-con-stato.png)

Il modo più generale per mantenere uno stato interno è quello dell'automa a stati.
La condizione dell'automa può essere usata come input per le regole.

È possibile, inoltre, costruire un modello parziale dell'ambiente, che permette di conoscere il risultato di azioni passate al momento della scelta.
L'introduzione di uno stato interno migliora la precedente debolezza di avere una versione parziale dell'ambiente.

Rimane il problema che, se l'ambiente non è statico, lo stato salvato può rappresentare un ambiente non coerente.
Inoltre il sistema di regole è molto stringente, poiché guidano fortemente le scelte dell'agente e non vengono modificate nel tempo.
Una soluzione è l'introduzione di goal.

#### Basati su goal

L'agente ha una visione dell'ambiente e ha conoscenza di come il mondo evolve in base alle azioni eseguite.
L'agente, dunque, può cercare una soluzione per raggiungere il goal.

![Agenti basati su goal](immagini/agente-basato-su-goal.png)

Rimane il problema di non totale conoscenza dell'ambiente, inoltre nel caso di raggiungimento di uno stato intermedio imprevisto ha la necessità di ri-pianificare le azioni.

Infine alcuni goal non sono raggiungibili p sono in conflitto tra loro.
Una possibile soluzione è quella di introdurre una misura sull'utilità degli stati raggiunti (e raggiungibili) dall'agente, tra questi stati ci sono anche i goal che vogliamo.

#### Basati su misura di utilità

L'agente conosce una mappa di utilità degli stati.
Avendo un numero molto alto di stati posso scegliere di cercare di spostarmi verso uno stato con utilità maggiore.
Questo metodo gestisce in modo intelligente la presenza di più agenti ed eventi con incertezza.

![Agenti basati su misura di utilità](immagini/agente-basato-su-misura-di-utilità.png)

C'è anche un il vantaggio di poter evitare di rimanere bloccati in un minimo locale di gratificazione, espandendo l'orizzonte di ricerca.

### Agenti che apprendono

L'agente costruisce regole sul comportamento in base ad una funzione di critica che viene generata provando le regole e valutandone il risultato rispetto a quello supposto e alle altre possibili regole.
L'elemento di learning propone nuove regole, l'elemento di performance le valuta in base a come si è comportato con l'ambiente e decide se tenerla o meno.
Il problem generator costruisce nuovi problemi/scenari, permettendo all'agente di provare nuove strade e nuove regole.

![Agenti che apprendono](immagini/agenti-che-apprendono.png)

Il modo più semplice di progettare un problem generator è quello di introdurre perturbazioni random.
Si tratta di una ricerca della massima copertura dello spazio delle ipotesi, ovvero dell'ambiente e delle azioni legali.

## Ricerca non informata

Agenti basati su goal sono agenti per la risoluzione di problemi ed operano nel seguente ordine:

- Formulazione del goal
- Formulazione del problema
  - Definizione degli stati
  - Definizione delle azioni legali
- Ricerca di una soluzione

## Risoluzione dei problemi

Ripeto fino al raggiungimento dell'obiettivo:

- Aggiornamento dello stato in base alle percezioni
- Se la sequenza delle azioni è vuota allora cerca di predire una soluzione
  - Formula un goal
  - Formula il problema che ha come soluzione le azioni per raggiungere il goal
  - Cerca una soluzione (sequenza di azioni)
  - Se la ricerca termina, allora aggiorna la sequenza di azioni, altrimenti non faccio nulla (NoOp)
- Eseguo la prima azione nella sequenza e la tolgo dalla lista

### Astrazione dello spazio degli stati per semplificare il problema

- Stato astratto
- Azione astratta
- Soluzione astratta

## Ricerca su grafi e alberi

### Differenza tra nodo e stato

- Uno stato è una rappresentazione di una configurazione fisica.
- Un nodo è una struttura dati che fa parte di un albero o un grafo e mantiene collegamenti con altri nodi.

### Ricerca su alberi

La ricerca di una soluzione è simile ad un albero: è possibile trovare lo stesso stato in più nodi dell'albero, poiché questo è infinito.
La ricerca parte dal nodo radice, legato allo stato iniziale, e si espande livello per livello.

### Ricerca su grafi

È possibile rappresentare la ricerca su un grafo: questo permette di indicare quali nodi sono già stati esplorati, evitandoli, ma ha problemi in caso di informazione parziale.

Hanno il vantaggio della separazione della frontiera, ma rischio di trovare soluzioni sub-ottimali se non scelgo adeguatamente il percorso da seguire.

## Algoritmi di ricerca

Seguono alcuni degli algoritmi di ricerca più utilizzati. L'implementazione vista è quella adatta per la ricerca su alberi, trattandosi del caso più semplice da implementare.

### Breadth First Search (BFS) - Ricerca in ampiezza

Partendo dal nodo radice, dato l'insieme delle foglie dell'albero, si espande la ricerca alla foglia che ha costo di cammino minore.

#### Proprietà BFS

- Se c'è una soluzione, BFS la trova ed è il percorso ottimo se uso una ricerca a costo uniforme;
- La frontiera cresce esponenzialmente ed occupa molta memoria.

### Depth First Search (DFS) - Ricerca in profondità

Partendo dal nodo radice, dato l'insieme delle foglie dell'albero, si espande la ricerca alla foglia che ha profondità maggiore, per poi eseguire backtracking nel caso di un vicolo cieco.

#### Proprietà DFS

- Occupa $(b-1)*\text{profondità}$ nodi in memoria, dove $b$ è il numero di figli per ogni nodo (breadth);
- Può non terminare;
- Posso applicare una ricerca iterativa in profondità fissando un altezza $h$ (algoritmo IDS).

### Iterative Depth first Search (IDS) - Ricerca in profondità iterativa

Eseguo la ricerca DFS fissando un limite massimo alla profondità di ricerca.
Se non trovo una soluzione all'altezza $h$, ripeto la ricerca con altezza $h+1$.

Per evitare di ricordare le iterazioni passate salviamo solo l'altezza del nodo limite.
Questo porta a ri-visitare gli stessi nodi visti in precedenza, ma si tratta di un overhead limitato dall'ordine di grandezza di $b$.

#### Proprietà IDS

- Può trovare soluzioni non ottimali, ma è una ricerca completa.

### Bidirectional Search (BS)

Applicabile quando la direzione di transizione degli stati può essere invertita

Eseguo la ricerca parallelamente, partendo dal nodo che rappresenta lo stato finale e dal nodo che rappresenta lo stato obiettivo.

Sfrutto uno degli algoritmi precedenti (in genere BFS) e cerco il primo nodo punto di contatto tra i due alberi di ricerca.

### Tabella riassuntiva

| Criterio | BFS      | Uniform cost                           | DFS      | Depth limited | IDS      | BS           |
| -------- | -------- | -------------------------------------- | -------- | ------------- | -------- | ------------ |
| Completo | Si (a)   | Si (a,b)                               | No       | No            | Si (a)   | Si (a,b)     |
| Tempo    | $O(b^d)$ | $O(b^{1+\lfloor C^*/\epsilon\rfloor})$ | $O(b^m)$ | $O(b^l)$      | $O(b^d)$ | $O(b^{d/2})$ |
| Spazio   | $O(b^d)$ | $O(b^{1+\lfloor C^*/\epsilon\rfloor})$ | $O(bm)$  | $O(bl)$       | $O(bd)$  | $O(b^{d/2})$ |
| Ottimale | Si (c)   | Si                                     | No       | No            | Si (c)   | Si (c,d)     |

Notazione:

- $b$ fattore di ramificazione;
- $d$ profondità della soluzione più vicina alla radice;
- $m$ profondità massima dell'albero di ricerca;
- $l$ limite di profondità.

- a: completa se $b$ finito;
- b: completa se un passo ha costo $\geq\epsilon>0$;
- c: ottima se i costi sono tutti identici;
- d: se entrambe le direzioni usando BFS.

## Ricerca informata

Una ricerca informata sfrutta informazioni note a priori sul problema da risolvere ed è basata sulla presenza di una funzione di valutazione della stima di desiderabilità di un nodo.

La frontiera è una coda ordinata in modo decrescente rispetto alla desiderabilità.

### Ricerca Best first

Ricerca greedy basata su una funzione di valutazione $f(n)$ per ogni nodo $n$, dove $f$ è una stima di desiderabilità dello stato rappresentato nel nodo $n$.

Utilizza una coda di priorità di espansione, ordinata in modo decrescente su $f$.

Non completa e rischia di bloccarsi in loop.
Tempo e spazio $O(b^m)$ ma migliora di molto con una buona euristica.
Non ottima.

### Ricerca greedy - Best First

Definiamo una funzione di valutazione sulla base di una funzione euristica:
$$
f(n)=\text{ stima del costo dal nodo $n$ al nodo goal più vicino}
$$

#### Esempio di funzione euristica

Definiamo $h_\text{SLD}(n) = \text{distanza in linea d'aria da $n$ alla posizione geografica target più vicina}$

La ricerca greedy espande il nodo che *appare* essere il più vicino al goal, secondo l'euristica, ovvero $f(n)=h(n)$.

#### Proprietà della ricerca greedy

- La ricerca non è completa e può restare intrappolata in cicli.
  Per evitare questo caso si possono controllare gli stati già visitati, in modo da non esplorarli nuovamente;
- Tempo in $O(b^m)$, ma l'uso di una buona euristica può dare miglioramenti enormi;
- Spazio in $O(b^m)$, poiché mantiene tutti i nodi in memoria;
- La ricerca non è ottima, non necessariamente trova la soluzione migliore dato che non stiamo considerando i pesi degli archi.

### Ricerca A*

Evita di espandere cammini che sono già più costosi.
Definisco la funzione di valutazione con $f(n) = g(n) + h(n)$ dove

- $g(n)$ è il costo già sostenuto per raggiungere $n$ dal nodo di partenza;
- $h(n)$ è il costo stimato da $n$ al goal, dato dalla funzione euristica;
- $f(n)$ è il costo totale stimato del cammino al goal che passa per $n$.

La ricerca A* usa una euristica *ammissibile* ovvero $\forall n.h(n) \leq h^*(n)$, dove $h^*(n)$ è il costo *effettivo* da $n$, a noi ignoto, e $h(G)=0$ con $G$ uno dei goal.
Un'euristica ammissibile non sovrastima mai il costo effettivo.

#### Esempio di euristica ammissibile

$h_\text{SLD}$, la distanza in linea d'aria tra due punti, è un'euristica ammissibile, poiché non sovrastima mai la distanza effettiva.

#### Ottimalità A* sugli alberi

Sia $G_2$ un goal sub-ottimale nella coda di espansione e $n$ un nodo non ancora espanso su un cammino verso un goal ottimo $G$.
$$
\begin{aligned}
   f(G_2)
      &= g(G_2) + h(G_2)   && \text{per definizione di della funzione $f$} \\
      &= g(G_2)            && \text{poiché $h(G_2)=0$}                     \\
      &> g(G)              && \text{poiché $G_2$ è sub-ottimo}              \\
      &\geq f(n)           && \text{poiché $h$ è ammissibile}              \\
\end{aligned}
$$
Poiché $f(G_2)>f(n)$, l'algoritmo A* non selezionerà mai il nodo $G_2$ per l'espansione.

##### Lemma sull'espansione di A*

A* espande i nodi in ordine di valore di $f$.

Questo è vero poiché gradualmente, aggiunge dei "contorni di $f$" dei nodi, dove il contorno $i$ possiede tutti i nodi con $f=f_i$, dove $f_i<f_{i+1}$.

#### Ottimalità A* sui grafi

Per garantire l'ottimalità di A* sui grafi ho necessità di utilizzare euristiche *consistenti* ovvero tali per cui $\forall n,n'.\ h(n)\leq c(n,a,n') + h(n')$ ovvero:
$$
\begin{aligned}
   f(n')
      &= g(n') + h(n')           && \text{per definizione della funzione $f$}                \\
      &= g(n) + c(n,a,n')+h(n')  && \text{espandendo $g(n')$ nelle componenti $g(n)$ e $c$}  \\
      &\geq g(n)+h(n)            && \text{applicando la disequazione sopra}                  \\
      &= f(n)                    && \text{per definizione di della funzione $f$}
\end{aligned}
$$
Dunque $f(n)$ è non decrescente lungo un cammino.

#### Proprietà di A*

- Completo, a meno che non ci sia un numero infinito di nodi con $f\leq f(G)$;
- Tempo: esponenziale in $[\text{errore relativo in }h\times\text{lunghezza della soluzione}]$;
- Spazio: Mantiene tutti i nodi in memoria;
- Ottimo: si, poiché non può espandere il contorno $f_{i+}$ finanche non ha espanso tutti i nodi nel contorno $f_i$.

A* espande tutti i nodi con $f(n)<C^*$ e alcuni con $f(n)=C^*$, ma non espande alcun nodo con $f(n)>C^*$.

#### Ottimalità di A*

A* non solo è ottimo, ma non esiste altro algoritmo ottimo che sicuramente espande meno nodi di A*, a meno di casi di parità sui nodi con $f$ uguale al valore ottimo.

Questo è vero poiché se così non fosse, allora tale algoritmo rischierebbe di non esplorare nodi ottimi, dunque non sarebbe un algoritmo ottimo.

### Iterative Deepening A* (IDA*)

A* ha occupazione esponenziale in spazio, quindi rischia di non essere applicabile a molti problemi interessanti.

Possiamo però applicare una soluzione simile a quella adottata per la ricerca DFS applicando un cutoff iterativo.

Vengono inseriti nella coda di espansione solo i nodi con valore di $f$ minore del cutoff.
Se il goal non viene trovato si aggiorna il valore di cutoff al minimo valore di $f$ dei nodi non inseriti nella coda.

### Recursive Best First Search (RBFS)

RBFS è un algoritmo ricorsivo che imita una ricerca in profondità, ma utilizza spazio lineare

- Tiene traccia dell'f-valore del miglior cammino che parte da uno degli avi;
- Se il nodo corrente supera l'f-valore alternativo, la ricorsione torna indietro al cammino alternativo;
- Durante il ritorno, si sostituisce l'f-valore di suoi nodi figli.

#### Proprietà di RBFS

- Complessità spaziale: $O(bd)$;
- Complessità temporale esponenziale nel caso pessimo;
- Ottimo se viene utilizzata una funzione euristica ammissibile.

Il problema di RBFS è l'uso di poca memoria, che porta alla rivalutazione di alcuni nodi durante la ricerca.
Per utilizzare tutta la memoria disponibile possiamo usare gli algoritmi MA* e SMA*.

### Simplified Memory-bounded A* (SMA*)

Simplified MA* procede come A*, espandendo la foglia migliore fino a quando la memoria è piena.
Successivamente, per poter proseguire, deve cancellare dalla memoria un nodo vecchio per far posto ad un nodo nuovo.

In particolare:

- Scarta sempre il nodo foglia peggiore, ovvero quello con f-valore più alto, che è in fondo alla coda;
- L'f-valore del nodo scartato viene riportato sul nodo padre;
- Si espande di nuovo il nodo padre quando tutti gli altri cammini sono peggiori.

Cosa succede se tutte le foglie hanno lo stesso f-valore?

SMA* rischierebbe di scegliere lo stesso nodo sia per la cancellazione che per l'espansione.
La politica adottata è rimuovere la foglia più vecchia ed espandere la foglia più recente.
Se queste coincidono, significa che la soluzione che passa da quel nodo non può essere contenuta dalla memoria disponibile, dunque è giusto rimuoverla.

#### Proprietà di SMA*

- Completo solo se la soluzione può essere contenuta in memoria;
- Ottima se la soluzione è raggiungibile, altrimenti restituisce la migliore soluzione raggiungibile.

## Costruzione sistematica di euristiche

Molte euristiche ammissibili possono essere derivate dal costo esatto di una soluzione di una versione rilassata del problema.

Parto dalla ricerca della soluzione ottima al problema e riduco i vincoli di questo per semplificarlo.
Trattandosi di una semplificazione, la soluzione del problema più semplice sarà al più tanto complessa quanto quella del problema originale.

### Esempio di costruzione sistematica di un euristica

Consideriamo il gioco dell'8, semplificazione del gioco del 15.
Vogliamo definire un'euristica sulla desiderabilità di uno stato del gioco.

- $h_1(n) =$ numero di tasselli in posizione errata;
- $h_2(n) =$ distanza Manhattan totale, ovvero la somma del numero di spostamenti per raggiungere la posizione desiderata per ogni tassello.

Nel primo caso abbiamo rilassato le regole sullo spostamento dei tasselli, permettendo qualsiasi spostamento, dunque $h_1$ corrisponde alla soluzione sul cammino più breve.

Nel secondo caso abbiamo rilassato le regole sullo spostamento dei tasselli, permettendo solo spostamenti sul quadrato adiacente, dunque $h_2$ corrisponde alla soluzione sul cammino più breve.

## Algoritmi di miglioramento iterativo

In molti problemi di ottimizzazione il cammino di una soluzione è irrilevante: quando cerchiamo una configurazione ottima, vogliamo solo sapere lo stato della soluzione finale.

Lo spazio degli stati è dato dall'insieme delle configurazioni complete, come ad esempio trovare la configurazione ottima o quella che soddisfa i vincoli.

In questi casi possiamo usare algoritmi di miglioramento iterativo, che mantengono un singolo stato *corrente* e tentano di migliorarlo.
Questi impiegano spazio costante e sono adatti sia per la ricerca online che quella offline.

### Esempio del commesso viaggiatore

Si parte con un percorso qualsiasi che raggiunga tutti i nodi.
Successivamente, si scambiano coppie fino a trovare una configurazione migliore.

### Hill-climbing

- Parto da una soluzione non ottimale;
- Genero gli stati vicini raggiungibili tramite le azioni disponibili;
- Valuto se uno di questi vicini è una soluzione migliore di quella attuale;
- Mi sposto sullo stato vicino migliore se esiste, altrimenti termino (massimo locale).

Problemi dell'Hill-climbing

- Massimi locali
- Plateau
- Ridge

#### Esempio delle n-regine

Vogliamo disporre $n$ regine su una scacchiera $n\times n$ senza che si minaccino, ovvero che non ci siano due regine sulla stessa riga, colonna o diagonale.

Le azioni disponibili sono i movimenti validi delle pedine sulla scacchiera.
L'obiettivo è minimizzare il numero di minacce.

Definiamo $h(s) =$ numero di coppie di regine che si attaccano a vicenda.
Ci sono $8\times 7$ possibili stati successori per ogni stato, supponendo che lo stato iniziale preveda tutte le regine su una riga orizzontale o verticale e che queste si spostino solo sull'asse ortogonale.

### Problemi di Hill-climbing

L'algoritmo di Hill-climbing incontra tre due tipi di situazioni problematiche:

#### Massimi locali

Quando potrebbero esistere massimi globali migliori, ma la funzione obiettivo rimane bloccata in un massimo locale.

In questi casi prevediamo le seguenti soluzioni:

- Hill-climbing stocastico: scegliere a caso tra tutte le mosse che migliorano $h$, eventualmente usando una probabilità di selezione proporzionale al miglioramento.
  Questo porta ad una convergenza più lenta, ma spesso trova soluzioni migliori.

- Hill-climbing con riavvio casuale: eseguire più ricerche a partire da stati iniziali diversi randomici. Se $p$ è la probabilità di trovare una soluzione ottima per una singola ricerca, il numero di ricerche atteso prima di trovare una soluzione ottima è $\frac{1}{p}$.

  **Dimostrazione**

  Consideriamo le variabili aleatorie binarie $\mathbf{x}_i$ rati che
  $$
  \mathbf{x}_i=\begin{cases}
     0 & \text{se la $i$-esima ricerca non trova una soluzione ottima} \\
     1 & \text{se la $i$-esima ricerca trova una soluzione ottima}     \\
  \end{cases}
  $$
  Sappiamo che $\forall i.\ P(\mathbf{x}_i=1)=p$ e che $\forall i.\ P(\mathbf{x}_i=0)=1-p$.
  Le variabili $\mathbf{x}_i$ sono mutuamente indipendenti, dunque possiamo modellare un processo di Bernoulli.

  La probabilità che la $k$-esima ricerca sia la prima a trovare una soluzione ottima è
  $$
  P((\forall j\in\{1,\ldots,k-1\}.\ \mathbf{x}_j=0)\land\mathbf{x}_k=1)=(1-p)^{k-1}p
  $$
  Il valore atteso della quantità che ci interessa è
  $$
  \begin{aligned}
     \sum_{k=1}^\infty k(1-p)^{k-1}p
         &=p\sum_{k=1}^\infty k(1-p)^{k-1} && \text{sposto $p$ fuori dalla sommatoria}\\
         &=-p\sum_{k=1}^\infty\frac{\partial(1-p)^k}{\partial p} && \text{applico la derivata}\\
         &=-p\frac{\partial}{\partial p}(\sum_{k=0}^\infty(1-p)^k-1) && \text{sposto la derivazione fuori dalla sommatoria} \\
         &=-p\frac{\partial}{\partial p}(\frac{1}{p}-1) && \text{sostituisco con il valore della sommatoria notevole} \\
         &=p\frac{1}{p^2} = \frac{1}{p} && \text{applico la derivata notevole}
  \end{aligned}
  $$
  Dunque il valore atteso di iterazioni per trovare la soluzione ottima è $\frac{1}{p}$.

#### Plateau

Quando la funzione obiettivo raggiungere un massimo locale piatto, da cui è difficile individuare punti migliori vicini.

In questo caso la soluzione tipica è una "mossa laterale": ci si sposta in uno stato con valore di $h$ identico e si cerca di esplorare lo spazio.
È ovviamente importante evitare cicli, per questo motivo, in genere, si fissa un numero massimo di mosse laterali consecutive.

#### Simulazione pratica del problema delle 8 regine

Il problema delle 8 regine ha $8^8$ stati, circa 17 milioni.

##### Hill-climbing standard

- Soluzione ottima viene trovata il 14% delle volte
- In media circa 4 passi per trovare una soluzione, altrimenti circa 3 passi in caso di soluzione sub-ottima

##### Hill-climbing con mosse laterali (non più di 100 consecutive)

- Soluzione ottima viene trovata il 94% delle volte
- In media circa 21 passi per trovare una soluzione, altrimenti circa 64 passi in caso di soluzione sub-ottima

##### Hill-climbing con riavvio casuale

- Soluzione ottima viene trovata con probabilità $p=0.14$, quindi circa $7$ ricerche per trovare una soluzione ottima, poiché $(1-p)/p=6,14$ fallimenti e 1 successo. Il numero di passi complessivo atteso è $3(1-p)/p+4=22.43$, con i valori 3 e 4 ottenuti dalle prove precedenti nel caso standard, rispettivamente per il caso sub-ottimale e per il caso ottimale.
- Applicando anche mosse laterali la soluzione ottima viene trovata con probabilità $p=0,94$, quindi circa $(1-p)/p+1=0.64$ fallimenti e 1 successo. Il numero di passi complessivo atteso è $64(1-p)/p+21=25,08$, con i valori 64 e 21 ottenuti dalle prove precedenti nel caso con mosse laterali, rispettivamente per il caso sub-ottimale e per il caso ottimale.

### Simulated annealing

L'idea alla base del simulated annealing è di evitare i massimi locali permettendo delle mosse cattive e di diminuirne la loro grandezza e frequenza gradualmente.

A temperatura fissata $T$ la probabilità di occupazione degli stati raggiunge la distribuzione di Boltzman
$$
p(x)=\alpha e^{\frac{E(X)}{kT}}
$$
Se $T$ viene diminuito abbastanza lentamente si raggiunge sempre lo stato migliore.

### Discesa di gradiente

Nel caso di spazi continui la scelta ideale è quella di spostarsi in uno stato nella direzione opposta al gradiente della funzione errore.

Si parte da un vettore di pesi random $\overset{\rightarrow}{w}$ e si calcola il gradiente $\nabla J\lbrack\overset{\rightarrow}{w}\rbrack\equiv\lbrack\frac{\partial J}{\partial w_0}\ldots,\frac{\partial J}{\partial w_n}\rbrack$.
Viene poi calcolato il delta di spostamento $\Delta\overset{\rightarrow}{w}=-\eta\nabla J\lbrack\overset{\rightarrow}{w}\rbrack$, o calcolato sul singolo peso $\Delta w_i=-\eta\frac{\partial J}{\partial w_i}$.
Infine si aggiorna il vettore pesi applicando il delta calcolato.

## Ricerca online

Quando il problema non è totalmente osservabile oppure è dinamico bisogna interagire con l'ambiente per estrarre informazione.
Non è possibile pianificare a tavolino tutte le possibili azioni; c'è la necessità per l'agente di alternare percezione e azione.
La ricerca online è particolarmente adatta a problemi di esplorazione.

Si assume che l'agente conosca solo quanto segue:

- Le azioni disponibili: immaginiamo una funzione $AZIONI(s)$ che dato uno stato $s$ restituisce le azioni che possono essere eseguite;
- Il costo per spostarsi da uno stato al seguente, rappresentabile come una funzione $c(s,a,s')$;
- Una funzione di test sull'obbiettivo, che indica se lo stato in input è un goal.

### Problemi della ricerca online

Poiché l'agente ha informazione parziale, rischia di finire in vicoli cechi spesso e, in generale, non è possibile evitare questo problema.

### Ricerca online tramite DFS

Il più semplice algoritmo applicabile alla ricerca online è DFS.
L'idea è di cercare in profondità finché non si raggiunge un goal; se si raggiunge un vicolo cieco, si fa backtracking fino al primo vincolo disponibile e si esplora un'altra via.

### Ricerca casuale

Un algoritmo altrettanto semplice per la ricerca online è quello della ricerca casuale.
In questo caso si sceglie casualmente tra le scelte disponibili.

È possibile applicare dei pesi sulle scelte, ad esempio favorendo le azioni non utilizzate fino a quel momento.
Si può dimostrare che la ricerca termina in passi finiti in spazi finiti, ma può essere molto lenta.

### Ricerca Learning Real-Time A* (LRTA*)

L'idea di questo algoritmo è memorizzare la miglior stima corrente $H(s)$ del costo per raggiungere il goal da ogni stato visitato.
Inizialmente $H(s)$ coincide con $h(s)$, ma successivamente viene aggiornata con l'esperienza.

![Ricerca LRTA*](immagini/ricerca-lrta-star.png)

![Algoritmo ricerca LRTA*](immagini/algoritmo-lrta-star.png)

## Algoritmi per giochi

Nel caso dei giochi il fattore che più influenza la risoluzione del problema è l'imprevedibilità dell'avversario: non è affatto detto che questo esegua la mossa perfetta ad ogni turno, perciò è necessario trovare una strategia, una soluzione che specifica una mossa per ogni possibile risposta dell'avversario.

Un altro importante fattore è il limite di tempo: nella maggior parte dei giochi il tempo di risposta è limitato, dunque non è possibile esplorare l'interezza dello spazio delle azioni ed è necessario approssimare.

### Tipi di giochi

I giochi si distinguono per il grado di informazione disponibile e per il loro determinismo.

- Informazione
  - Ad informazione completa
  - Ad informazione parziale

- Determinismo
  - Deterministico
  - Non deterministico

### Minmax

Ad ogni turno ciascun giocatore cerca di massimizzare il proprio punteggio e minimizzare il punteggio dell'avversario.
Per fare ciò esploro un albero che rappresenta gli stati di gioco.
Se entrambi i giocatori sono *perfetti*, ad ogni turno cercheranno di trovare la foglia che da loro il migliore risultato, noto che ad ogni livello ci sarà una massimizzazione o una minimizzazione ed una scelta non controllata, fatta dall'avversario.

- È completo se l'albero di gioco è finito.
- È ottimo se l'avversario è ottimo.
- Ha complessità temporale $O(b^m)$ e spaziale $O(bm)$ con esplorazione depth-first.

Minmax è in grado di gestire anche più giocatori, alternando un numero maggiore di turni con scelta non controllata a quello con scelta controllata.

### $\alpha$-$\beta$ pruning

Dato $\alpha$ miglior valore per MAX trovato fino a quel momento al di fuori del cammino corrente, se V è peggiore di $\alpha$, MAX lo eviterà, dunque è possibile potare il ramo corrispondente.
Similmente si può definire $\beta$ per MIN.

Il pruning non modifica il risultato finale, dato che i rami potati non vengono utilizzati.
Un buon ordinamento delle mosse migliora l'efficacia del pruning.
Con ordine perfetto viene esplorato solo il primo nodo per ogni massimizzazione (o minimizzazione) e la complessità di tempo diventa $O(b^{m/2})$, raddoppiando la profondità di ricerca attesa dato un certo limite di tempo.

#### Analisi del best case con ordine perfetto

Per avere il valore esatto di uno stato occorre conoscere il valore esatto di utilità per uno stato figlio e conoscere un bound sulla utilità di tutti gli stati figli rimanenti.

Per derivare un bound sulla utilità di uno stato occorre conoscere il valore esatto di utilità per uno stato figlio.

Poniamo $E(d)$ il numero minimo di stati da considerare per conoscere il valore esatto di utilità di uno stato a distanza $d$ dagli stati terminali e $B(d)$ il numero minimo di stati da considerare per conoscere un bound sul valore di utilità di uno stato a distanza $d$ dagli stati terminali.
Allora:
$$
\begin{aligned}
E(d+1) &=E(d)+(b-1)B(d) \\
B(d+1) &=E(d) \\
E(0)&=B(0)=1 \\
E(d-1)&<E(d)
\end{aligned}
$$
Espandendo per $E(d+2)$ troviamo:
$$
\begin{aligned}
E(d+2)
&=E(d+1)+(b-1)B(d+1) && \text{per definizione di $E(d+1)$} \\
&=(E(d)+(b-1)B(d))+(b-1)B(d+1) && \text{per definizione di $E(d+1)$} \\
&=(E(d)+(b-1)B(d))+(b-1)E(d) && \text{per definizione di $B(d+1)$} \\
&=bE(d)+(b-1)B(d) && \text{espandendo le moltiplicazioni} \\
&=bE(d)+(b-1)E(d-1) && \text{per definizione di $B(d)$}
\end{aligned}
$$
Dunque:
$$
\begin{aligned}
E(d+2)
&=bE(d)+(b-1)E(d-1) \\
&<(2b-1)E(d) \\
&<2bE(d)
\end{aligned}
$$
Infine:
$$
E(m)\leq(\sqrt{2b})^m=(\sqrt{2})^mb^\frac{m}{2}=2^\frac{m}{2}b^\frac{m}{2}
$$

### Limiti alle risorse

Per limitare l'esplorazione eseguita in modo da massimizzarne l'efficacia dati i limiti sulle risorse temporali e di memoria si possono seguire gli approcci:

- Test di taglio: limite alla profondità sull'esplorazione.
  È possibile utilizzare anche ricerca di quiescenza, ovvero una ricerca che arresta l'esplorazione di un ramo quando è improbabile che la soluzione data da questo possa essere invertita da una mossa futura;
- Funzione di valutazione: stima della desiderabilità dello stato.
  Non abbiamo quasi mai bisogno dei valori esatti, ma solo che l'ordine della desiderabilità sia corretto per poter scegliere come esplorare i nodi.
  
#### Esempio di funzione di valutazione

Nel gioco degli scacchi una funzione di valutazione tipica è quella che calcola una somma pesata lineare di feature, ovvero quante e quali pedine sono presenti sulla scacchiera.

## Giochi non deterministici

Nei giochi non deterministici sono presenti eventi casuali che non sono controllati da nessuno dei giocatori, ad esempio il lancio di dadi o la mescolanza di carte.

Il funzionamento è similare al caso deterministico, ma nella rappresentazione ad albero dobbiamo aggiungere dei nodi per cui la scelta dell'azione da intraprendere è probabilistica e ciascuna azione ha una certa probabilità nota a priori.

Possiamo applicare gli stessi algoritmi Minmax e $\alpha$-$\beta$ pruning, propriamente adattati, considerando che dovremo esplorare necessariamente tutti i sottoalberi di ogni nodo non deterministico, poiché nessuno dei giocatori ha possibilità di influenzare la scelta dell'azione in quel caso.

Minmax lavora sul valore atteso del punteggio in ciascuno degli stati, invece del valore reale.
$$
ExpectiMinmaxValue(state)=\sum_{s\in Successors(state)}P(s)ExpectiMinmaxValue(s)
$$

Possiamo comunque eseguire una potatura dei sottoalberi in stile $\alpha$-$\beta$, nel caso i possibili valori ottenuti siano strettamente peggiori di quelli altrimenti raggiungibili in un altro ramo.
Se riusciamo a dimostrare che un sotto-albero produce un risultato in un intervallo strettamente peggiore del migliore possibile, allora tale sotto-albero può essere scartato.

È possibile applicare trasformazioni lineari e positive alla funzione EVAL, basta che il valore ottenuto sia proporzionale al valore aspettato di guadagno.

## Giochi ad informazione parziale

Solo una parte dell'informazione sullo stato del gioco è disponibile.
Possiamo pensare di calcolare la probabilità di uscita di ogni possibile stato iniziale, calcolarne il valore atteso del punteggio e utilizzare Minmax per valutare la scelta migliore.
Da notare che se una scelta è ottima per ogni partita allora essa è sempre ottima.

### Esempio gioco ad informazione parziale: Bridge

Genero 100 partite consistenti con informazioni sulle dichiarazioni di presa e applico l'azione che in media ha portato al risultato migliore (più vincite) nelle simulazioni.

### Analisi corretta

L'intuizione che il valore di ogni azione è la media dei suoi valori in tutti i possibili stati è sbagliata.
Nel caso di informazione non completa, il valore di un'azione dipende dallo stato di informazione o stato di credenza (belief state) in cui si trova l'agente.

È possibile generare e ricercare all'interno di un albero di stati di credenza e conduce a comportamenti razionali come:

- agire con lo scopo di ottenere informazione;
- trasmettere informazione al proprio compagno di gioco;
- agire in modo casuale per minimizzare la perdita di informazione, ovvero rivelare informazione agli avversari.

## Agenti basati su goal

La logica fornisce un possibile formalismo per codificare la conoscenza necessaria (ovali) ad un agente, qualunque sia la sua architettura.

Definiamo la **base di conoscenza** come un insieme di sentenze in un linguaggio formale.

Utilizziamo un approccio dichiarativo per la costruzione di un agente:

- Diciamo ad esso quello che ha bisogno di sapere;
- L'agente può chiedere a se stesso cosa fare.
  Le risposte che cerca dovrebbero seguire dalla base di conoscenza.

Gli agenti possono essere descritti al **livello della conoscenza**, cioè per quello che sanno, indipendentemente dall'implementazione, oppure a **livello implementativo**, cioè considerando le strutture dati della KB e gli algoritmi che la manipolano.

I'algoritmo per scegliere l'azione da eseguire è il seguente:

- Dire all'agente delle sentenze, derivate dai dati percettivi, aggiornando la KB;
- L'agente si chiede che azione eseguire e la sceglie a seconda della KB;
- L'agente segna nella KB il risultato dell'azione, includendo un timestamp;

Affinché l'algoritmo funzioni l'agente deve essere capace di:

- Rappresentare stati, azioni, ecc.
- Incorporare nuove percezioni
- Aggiornare le rappresentazioni interne del mondo
- Dedurre le azioni appropriate da intraprendere

### Modelli

I logici tipicamente pensano in termini di modelli, che formalmente sono modi strutturati rispetto ai quali si può valutare la verità.

Diciamo che $m$ è un modello di una sentenza $\alpha$ se $\alpha$ è vera in $m$. Inoltre definiamo $M(\alpha)$ come l'insieme di tutti i modelli di $\alpha$.

Allora $KB \vDash \alpha \iff M(KB)\subseteq M(\alpha)$, ovvero $\alpha$ segue dalla KB se e solo se l'insieme dei modelli per cui $\alpha$ è valido contiene l'insieme per cui la KB è valida.

#### Esempio di inferenza

Assumiamo $KB = \{\text{Inter ha vinto},\text{Milan ha vinto}\}$ e $\alpha = \text{Inter ha vinto}$.

$KB\vDash\alpha$ è vero.

#### Esempio di modello: il mondo dei Wumpus

##### Ambiente del mondo dei Wumpus

- Il mondo dei Wumpus è una scacchiera;
- Una delle caselle contiene un tesoro, mentre altre sono buche (trappole) o ospitano un Wumpus;
- I Wumpus puzzano, dunque le caselle adiacenti (a croce) puzzano;
- Le trappole sono ventilate, dunque nelle caselle adiacenti (a croce) si sente il vento;
- L'eroe può lanciare una freccia in ogni direzione (a croce) che uccide i Wumpus, ma solo se nella casella adiacente;
- La freccia può essere usata una sola volta.
- L'eroe può raccogliere l'oro solo se si trova nella sua stessa casella.

##### Mistura di prestazione del mondo dei Wumpus

- +1000 se troviamo il tesoro;
- -1000 se moriamo;
- -1 per ogni spostamento;
- -10 per l'utilizzo della freccia.

##### Sensori del mondo dei Wumpus

- Brezza;
- Puzza;
- Presenza d'oro nella casella.

##### Attuatori del mondo dei Wumpus

- Spostamento nelle quattro direzioni;
- Lanciare la freccia;
- Raccogliere l'oro.

##### Esempio pratico mondo dei Wumpus

Note le informazioni sullo spazio vicino possiamo inferire la presenza e la posizione di Wumpus e trappole e scegliere se una casella è sicura o no.

![Esempio pratico mondo dei Wumpus 1](immagini/esempio-pratico-mondo-dei-wumpus-1.png)

Data $KB=\text{regole del mondo dei Wumpus} + \text{osservazioni}$ e la query $\alpha_1 = [1,2]\text{ è sicuro}$.

$KB\vDash\alpha_1$, provato dal model checking (test sui modelli).

![Esempio pratico mondo dei Wumpus 2](immagini/esempio-pratico-mondo-dei-wumpus-2.png)

Data $KB=\text{regole del mondo dei Wumpus} + \text{osservazioni}$ e la query $\alpha_2 = [2,2]\text{ è sicuro}$.

$KB\not\vDash\alpha_2$.

### Inferenza per mezzo di enumerazione

Una enumerazione a scandaglio (Depth-first) di tutti i modelli è corretta e completa.

![Algoritmi di enumerazione di KB](immagini/algoritmo-enumerazione-knowledge-base.png)

Tempo $O(2^n)$ per $n$ simboli; il problema è co-NP-completo.

### Metodi di prova

I metodi di prova si suddividono in:

- Applicazione di regole di inferenza
  - Generazione (corretta) di nuove sentenze a partire da quelle vecchi;
  - Prova = sequenza di applicazione di regole di inferenza;
  - Si usano regole di inferenza come operatori in un algoritmo standard di ricerca;
  - Tipicamente richiede la rappresentazione dell sentenze in una forma normale.
- Model checking
  - Enumerazione della tabella di verità, esponenziale in $n$;
  - Backtracking migliorato, Es. Davis-Putnam-Logemann-Loveland;
  - Ricerca euristica nello spazio dei modelli (corretta ma incompleta), Es. algoritmi hill-climbing basati sulla minimizzazione dei conflitti.

### Forward e backward chaining

#### Forma di Horn ristretta

Consideriamo KB come una congiunzione di Clausole di Horn.

Una clausola di Horn è nella forma:

- simbolo;
- (congiunzione di simboli)$\implies$simbolo.

##### Esempi di forme di Horn ristrette

- $A$
- $C\implies B$
- $A\land C\implies B$
- $C\land(A\implies B)\land(C\land D\implies B)$

#### Modus Ponens per la forma di Horn

Il Modus Ponens è completo per basi di conoscenza in forma di Horn.
$$
\frac{\alpha_1,\ldots,\alpha_n,\ \ \ \ \alpha_1\land\ldots\land\alpha_n\implies\beta}{\beta}
$$

Può essere usato con forward chaining o backward chaining.
Questi algoritmi sono molto immediati e hanno complessità lineare in tempo.

#### Forward Chaining

L'idea del Forward Chaining è applicare ogni regola le cui premesse sono soddisfatte in KB.
I risultati ottenuti vengono aggiunti alla KB.
La sequenza viene ripetuta finché la query non viene derivata o finché non ci sono più implicazioni da verificare.

##### Completezza Forward Chaining

1. FC raggiunge un punto fisso dove nessuna nuova sentenza atomica è derivata;
1. Si consideri lo stato finale come un modello $m$, assegnando vero/falso ai simboli;
1. Ogni clausola nella KB originale è vera in $m$, quindi $m$ è un modello per KB.

   **Dimostrazione**: Per assurdo supponiamo che la clausola $\alpha_1\land\ldots\land\alpha_k\implies b$ sia falsa in $m$, allora $\alpha_1\land\ldots\land\alpha_k$ è vera in $m$ e $b$ è falsa in $m$, dunque l'algoritmo non ha raggiunto un punto fisso;
1. Se $KB\vDash q$, $q$ è vera per ogni modello in KB, incluso $m$.

#### Backward chaining

L'idea del Backward Chaining è di partire dalla query $q$.
Per provare $q$ controlliamo se $q$ è già conosciuta o provata in un passo precedente.
In caso contrario provo a derivare tutte le premesse di una regola che deriva $q$.

Per evitare cicli si controlla se un nuovo sotto-goal è già presente nella pila dei goal.

Per evitare di ripetere del lavoro controlla se un sotto-goal è stato già provato vero o se è già fallito.

#### Confronto tra FC e BC

FC è data-driven, guidato dai dati, adatto all'elaborazione inconscia, automatica, come il riconoscimento di oggetti o le decisioni di routine.
Rischia di eseguire molto lavoro irrilevante per il goal.

BC è goal-driven, guidato dal goal, appropriato per il problem solving, alla ricerca di soluzioni a domande.
La complessità di BC può essere molto minore che lineare nella dimensione di KB.

### Forma Normale Congiuntiva (CNF universale)

La Forma Normale Congiuntiva sfrutta congiunzioni di disgiunzioni di letterali per descrivere la KB.

#### Esempi di Forme Normali Congiuntive

- $A \land B$
- $A\lor\neg B$
- $(A\lor\neg B)\land(B\lor\neg C\lor\neg D)$

#### Risoluzione della regola di inferenza per CNF

La risoluzione è completa per la logica proposizionale.
Si sfrutta una forma del tipo:
$$
\frac{l_1\lor\ldots\lor l_k,\ \ \ \ m_1\lor\ldots\lor m_n}{l_1\lor\ldots\lor l_{i-1}\lor l_{i+1}\lor\ldots\lor l_k\lor m_1\lor\ldots\lor M_{j-1}\lor m_{j+1}\lor\ldots\lor m_n}
$$
dove $l_i$ e $m_j$ sono letterali complementari, ad esempio
$$
\frac{P_{1,3}\lor P_{2,2}\ \ \ \ \neg P_{2,2}}{P_{1,3}}
$$
La risoluzione è corretta e completa per la logica proposizionale.

#### Conversione in CNF

Consideriamo $B_{1,1}\iff(P_{1,2}\lor P_{2,1})$

1. Eliminare $\iff$, rimpiazzando $\alpha\iff\beta$ con $(\alpha\implies\beta)\land(\beta\implies\alpha)$:
   $$(B_{1,1}\implies(P_{1,2}\lor P_{2,1}))\land((P_{1,2}\lor P_{2,1})\implies B_{1,1})$$
1. Eliminare $\implies$, rimpiazzando $\alpha\implies\beta$ con $\neg\alpha\lor\beta$:
   $$(\neg B_{1,1}\lor P_{1,2}\lor P_{2,1})\land(\neg(P_{1,2}\lor P_{2,1})\lor B_{1,1})$$
1. Spostare $\neg$ all'interno usando le regole di De Morgan e la doppia negazione:
   $$(\neg B_{1,1}\lor P_{1,2}\lor P_{2,1})\land((\neg P_{1,2}\land\neg P_{2,1})\lor B_{1,1})$$
1. Applicare la legge distributiva, $\lor$ su $\land$, e portare tutto su un livello:
   $$(\neg B_{1,1}\lor P_{1,2}\lor P_{2,1})\land(\neg P_{1,2}\lor B_{1,1})\land(\neg P_{2,1}\lor B_{1,1})$$

#### Algoritmo di risoluzione per CNF

L'algoritmo usa una prova per contraddizione, cioè mostra che $KB\land\neg\alpha$ è insoddisfacibile.

![Algoritmo di risoluzione per CNF](immagini/algoritmo-risoluzione-cnf.png)

PL-RESOLVE($C_i$,$C_j$) restituisce l'insieme dei risolventi ottenuti applicando la regola di risoluzione in tutti i modi possibili per le clausole $C_i$ e $C_j$.

##### Completezza della risoluzione

Il teorema di completezza per la risoluzione nella logica proposizionale è chiamato ground resolution theorem:

Se un insieme di clausole $S$ è insoddisfacibile, allora la chiusura della risoluzione di tali clausole $RC(S)$ contiene la clausola vuota.

- La chiusura della risoluzione di un insieme di clausole corrisponde all'insieme ottenuto come punto fisso dall'applicazione ripetuta della risoluzione.
In caso di terminazione con fallimento dell'algoritmo visto, corrisponde all'insieme $clauses$ alla fine dell'esecuzione.

La dimostrazione del teorema si ottiene dimostrando quanto segue:

- Se la chiusura $RC(S)$ non contiene la clausola vuota, allora $S$ è *soddisfacibile*.

Se $RC(S)$ non contiene la clausola vuota, allora si può costruire un modello per $S$.
Infatti, si dia un ordine arbitrario ai simboli di predicato che compaiono in $S$, ottenendo $P_1,\ldots,P_k$.
Poi si segua la seguente procedura per costruire il modello:

- Per ogni $i$ da $1$ a $k$:
  - Se esiste una clausola in $RC(S)$ contenente $\neg P_i$ tale che tutti gli altri letterali della clausola sono falsi a causa del valore di verità già assegnato ai $P_1,\ldots,P_{i-1}$, allora assegna valore di verità *falso* a $P_i$;
  - Altrimenti assegna valore di verità *vero* a $P_i$.

Mostriamo ora che tale procedura termina sempre con un modello per $S$.

Facciamo tale dimostrazione per induzione su $i$.
Supponiamo che sia possibile costruire il modello parziale per i simboli fino a $P_{i-1}$ e mostriamo che tale modello può essere esteso per i simboli fino a $P_i$.

Nel caso base $i=1$, in $S$ non possono necessariamente essere presenti simultaneamente sia la clausola $P_1$ e $\neg P_1$, altrimenti $RC(S)$ conterrebbe la clausola vuota.
Quindi per $i=1$ la procedura descritta si può applicare senza problemi: $P_1\leftarrow falso$ se è presente $\neg P_1$, altrimenti $P_1\leftarrow vero$.

Per ipotesi induttiva vera per $i-1$, consideriamo una clausola $C$ in $RC(S)$ che contiene $P_i$.
Si hanno problemi ad assegnare un valore di verità a $P_i$ solo se

- $C\equiv B\lor\neg P_i$ con $B$ clausola che contiene solo simboli $P_j$ con $j<i$;
- Esiste in $RC(S)$ una clausola $C'\equiv B'\lor P_i$ con $B'$ clausola che contiene solo simboli $P_j$ con $j<i$.

Ma se questo succede, in $RC(S)$ deve essere presente anche la clausola $B\lor B'$, che contiene solo simboli $P_j$ con $j<i$, altrimenti $RC(S)$ non è la chiusura, ed a causa dell'ipotesi induttiva, l'assegnamento parziale fino a $P_{i-1}$ non può rendere falsa sia $B$ che $B'$.
Quindi, se è falsa $B$, $P_i\leftarrow falso$, se invece è falsa $B'$, allora $P_i\leftarrow vero$, ottenendo un modello parziale per i simboli fino all'indice $i$.

Si conclude che quando $i$ raggiunge $k$, otteniamo un modello completo per $S$ e quindi abbiamo dimostrato che $S$ è soddisfacibile.

## Logica del Primo Ordine

Mentre la Logica Proposizionale assume che il mondo contenga fatti, sentenze certe, la Logica del Primo Ordine, come anche il linguaggio naturale, assume che il mondo contenga:

- Oggetti
  - Persone, case, numeri, teorie, Pinco Pallino, colori, partite, guerre, secoli, ...
- Relazioni
  - Rosso, rotondo, finto, primo, fratello di, più grande di, dentro, parte di, ha colore, occorso dopo, possiede, ...
- Funzioni
  - padre di, miglior amico, secondo tempo di, inizio di, ...

### Verità nella Logica del Primo Ordine

Le sentenze sono vere rispetto ad un modelle ed una interpretazione:

- Il modello contiene almeno un oggetto (elementi del dominio) e relazioni fra loro;
- L'interpretazione specifica i referenti per
  - simboli di costanti, che rappresentano gli oggetti;
  - simboli di predicati, che rappresentano le relazioni;
  - simboli di funzione, che rappresentano relazioni funzionali.

Una sentenza atomica è della forma $predicato(termine_1,\ldots,termine_n)$ ed è vera se e solo se gli oggetti riferiti da $termine_1,\ldots,termine_n$ sono nella relazione riferita dal predicato.

![Esempio logica del Primo Ordine](immagini/esempio-logica-primo-ordine.png)

In questo esempio *R* e *J* sono **oggetti** persona, *fratello* e *sulla testa* è sono **relazioni**, e *gamba sinistra* è una **funzione**.

Si possono enumerare i modelli per il vocabolario di una data KB.

Per ogni numero di elementi di dominio $n$, da $1$ a $\infty$
   Per ogni predicato $k$-ario $P_k$ nel vocabolario
      Per ogni possibile relazione $k$-aria su $n$ oggetti
         Per ogni simbolo di costante $C$ nel vocabolario
            Per ogni scelta di referente per $C$ da $n$ oggetti $\ldots$

Calcolare le conseguenze logiche enumerando i modelli non è facile.

### Quantificatori Universale ed Esistenziale

Il più grande vantaggio della Logica del Primo Ordine, rispetto alla Logica Proposizionale è la possibilità di descrivere modelli il cui numero di elementi è indefinito o infinito.

La Logica del Primo Ordine permette di usare il quantificatore universale $\forall$ per indicare sentenze valide per qualsiasi variabile:
$$
\forall\langle variabili\rangle\langle sentenza\rangle
$$
**Esempio**:
Chiunque a Padova è intelligente si rappresenta con $\forall x\ Luogo(x,Padova)\implies Intelligente(x)$

Il predicato $\forall x\ P$ è vero in un modello $m$ se e solo se $P$ è vero essendo $x$ ogni possibile oggetto nel modello.

In prima approssimazione, equivale alla congiunzione di istanziazioni di $P$.
In generale $\forall$ è usato con il connettivo $\implies$.

La Logica del Primo ordine permette di usare il quantificatore esistenziale $\exists$ per indicare le sentenze per cui esiste almeno una variabile per cui queste sono vere:
$$
\exists\langle variabili\rangle\langle sentenza\rangle
$$

**Esempio**:
Qualcuno a Bologna è intelligente si rappresenta con $\exists x\ Luogo(x,Bologna)\land Intelligente(x)$

Il predicato $\exists\ x P$ è vero in un modello $m$ se e solo se $P$ è vero essendo $x$ *qualche* possibile oggetto nel modello

In prima approssimazione, equivalente alla disgiunzione di istanziazioni di $P$
In generale $\exists$ è usato con il connettivo $\land$.

### Proprietà dei quantificatori

- $\forall x\ \forall y \cong \forall y\ \forall x$;
- $\exists x\ \exists y \cong \exists y\ \exists x$;
- $\exists x\ \forall y \not\cong \forall y\ \exists x$;
- $\forall x\ P(x) \cong \neg\exists x\ \neg P$;
- $\exists x\ P(x) \cong \neg\forall x\ \neg P$.

### Inferenza: Instanziazione Universale (UI)

Ogni instanziazione di una sentenza quantificata universalmente è conseguenza logica di quest'ultima:
$$
\frac{\forall v\ \alpha}{\text{Subst}(\{v/g\},\alpha)}
$$

**Esempio**: $\forall x\ Re(x)\land Avido(x)\implies Malvagio(x)$ diventa $Re(Giovanni)\land Avido(Giovanni)\implies Malvagio(Giovanni),\\
Re(Riccardo)\land Avido(Riccardo)\implies Malvagio(Riccardo),\\
Re(Padre(Giovanni))\land Avido(Padre(Giovanni))\implies Malvagio(Padre(Giovanni)),\\
\ldots$

### Inferenza: Instanziazione Esistenziale (EI)

Per ogni sentenza $\alpha$, variabile $v$ e simbolo costante $k$ che non appare in nessuna parte della base di conoscenza:
$$
\frac{\exists v\ \alpha}{\text{Subst}(\{v/k\},\alpha)}
$$

**Esempio**: $\exists x\ Corona(x)\land SullaTesta(x,Giovanni)$ diventa $Corona(C_1)\land SullaTesta(C_1,Giovanni)$ a patto che $C_1$ sia un nuovo simbolo di costante, chiamato **costante di Skolem**.

**Esempio**: $\exists x\ \partial(x^y)/\partial y=x^y$ diventa $\partial(e^y)/\partial y=e^y$ a patto che $e$ sia un nuovo simbolo costante.

### Inferenza: applicazione delle istanziazioni

**UI** può essere applicata più volte per *aggiungere* nuove sentenze; la nuova KB è logicamente equivalente alla vecchia

**EI** può essere applicata solo una volta per *rimpiazzare* la sentenza esistenziale; la nuova KB non è equivalente alla vecchia, ma è soddisfacibile se e solo se la vecchia KB era soddisfacibile.

### Riduzione alla'inferenza proposizionale

Supponiamo che KB contenga solo le seguenti sentenze:

- $\forall x\ Re(x)\land Avido(x)\implies Malvagio(x)$;
- $Re(Giovanni)$;
- $Avido(Giovanni)$;
- $Fratello(Riccardo, Giovanni)$.

Instanziando la sentenza universale in tutti i possibili modi, si ottiene:

- $Re(Giovanni)\land Avido(Giovanni)\implies Malvagio(Giovanni)$;
- $Re(Riccardo)\land Avido(Riccardo)\implies Malvagio(Riccardo)$;
- $Re(Giovanni)$;
- $Avido(Giovanni)$;
- $Fratello(Riccardo, Giovanni)$.

La nuova KB è preposizionalizzata: i simboli proposizionali sono:

- $Re(Giovanni)$
- $Avido(Giovanni)$
- $Malvagio(Giovanni)$
- $Re(Giovanni)$
- $\ldots$

### Riduzione

Una sentenza ground è conseguenza logica della nuova KB se e solo se è conseguenza logica della KB originaria.

**Affermazione**: ogni Logica del Primo Ordine KB può essere proposizionalizzata in modo da preservarne le seguenze logiche.

**Idea**: proposizionalizzare sia KB che la query, applicare la risoluzione e restituire il risultato.

**Problema**: con i simboli di funzione, c'è un numero infinito di termini ground, ad esempio $Padre(\ldots(Padre(Giovani)\ldots)$

**Teorema: Herbrand (1930)** Se una sentenza $\alpha$ è conseguenza logica di una Logica del Primo Ordine KB, essa è conseguenza logica di un sottoinsieme finito della KB proposizionale.

**Idea**:

- Per ogni $n$ da $0$ a $\infty$:
  - Creare una KB proposizionale con termini di profondità $n$;
  - Controllare se $\alpha$ è conseguenza logica della KB considerata.

**Problema**: funziona solo se $\alpha$ è conseguenza logica, cicla all'infinito se $\alpha$ non è conseguenza logica

**Teorema: Turing (1936), Church (1936)** Entailment in Logica del Primo Ordine è semi-decidibile.

### Problemi con la proposizionalizzazione

La proposizionalizzazione sembra generare tante sentenze irrilevanti: con $p$ predicati $k$-ari e $n$ costanti, ci sono $p\cdot n^k$ istanziazioni.

### Unificazione

Si può ottenere l'inferenza immediatamente se possiamo trovare una sostituzione $\theta$ tale che $Re(x)$ e $Avido(y)$ corrispondano a $Re(Giovanni)$ e $Avido(Giovanni)$

$\theta = \{x/Giovanni,y/Giovanni\}$ va bene.

$\text{Unify}(\alpha,\beta)=\theta\text{ if }\alpha\theta=\beta\theta$

![Algoritmo di unificazione](immagini/algoritmo-di-unificazione.png)

### Modus Ponens Generalizzato

$$
\frac{p_1',\ldots,p_n'\ \ \ \ (p_1\land\ldots\land p_n\implies q)}{q\theta}
$$
dove $p_i'\theta=p_i\theta$ per tutte le $i$.
$$
\begin{aligned}
   P_1'\text{ è }Re(Giovanni) &\ \ \ \ \ && p_1 \text{ è } Re(x) \\
   P_2'\text{ è }Avido(y) &&& p_2 \text{ è } Avido(x) \\
   \theta\text{ è }\{x/Giovanni,y/Giovanni\} &&& q \text{ è } Malvagio(x) \\
   q\theta\text{ è }Malvagio(Giovanni) &&& \\
\end{aligned}
$$
GMP usato con KB composto di clausole definite, ovvero con esattamente un letterale positivo.
Sia assume che tutte le variabili siano quantificate universalmente.

### Algoritmo di Forward Chaining per FOL

![Algoritmo di Forward Chaining per FOL](immagini/algoritmo-fc-fol.png)

#### Proprietà del Forward Chaining per FOL

- Corretto e completo per clausole definite di primo ordine, la dimostrazione è simile a quella per la logica proposizionale;
- Datalog = clausole definite del primo ordine + nessuna funzione
- FC termina per Datalog in un numero di iterazioni polinomiale, al più $p\cdot n^k$ letterali.
- In generale può non terminare se $\alpha$ non è conseguenza logica, ciò è inevitabile, come provato dal teorema sull'Entailment, poiché il problema è semi-decidibile.

#### Esempio di KB del Forward Chaining

*KB*: La legge in America dice che è un crimine per un americano vendere armi a nazioni ostili.
La nazione Nono, nemico dell'America, possiede alcuni missili, venduti alla nazione dal Colonnello West, che è americano.

*Query*: Provare che il Colonnello West è un criminale

**Prova**:

- è un crimine per un americano vendere armi a nazioni ostili:
  $$
  American(x)\land Weapon(y)\land Hostile(z)\land Sells(x,y,z)\implies Criminal(x)
  $$
- Nono possiede dei missili, ovvero $\exists x\ Owns(Nono,x)\land Missile(x)$:
  $$
  Owns(Nono,M_1)\land Missile(M_1)
  $$
- Tutti i missili gli sono stati venduti dal Colonnello West:
  $$
  Missile(x)\land Owns(Nono,x)\implies Sells(West,x,Nono)
  $$
- I missili sono armi:
  $$
  Missile(x)\implies Weapon(x)
  $$
- Un nemico dell'America è ostile:
  $$
  Enemy(x,America)\implies Hostile(x)
  $$
- West è americano:
  $$
  American(West)
  $$
- La nazione Non è un nemico dell'America
  $$
  Enemy(Nono,America)
  $$

Segue la soluzione tramite forward chaining

![Esempio KB America](immagini/esempio-kb-america-fc.png)

#### Efficienza del Forward Chaining

Semplice osservazione: non c'è bisogno di match-are una regola all'iterazione $k$ se una premessa non è stata aggiunta all'iterazione $k-1$, dunque è più efficiente match-are ogni regola le cui premesse contengono un letterale appena aggiunto.

Poiché il matching è costoso è utile indicizzare la base di dati, permettendo il recupero di fatti conosciuti in $O(1)$.
Ad esempio la query $Missile(x)$ deve recuperare velocemente $Missile(M_1)$

Matching di premesse congiuntive rispetto a fatti conosciuti è NP-hard.

Il Forward Chaining è largamente utilizzato in basi di dati deduttive.

### Algoritmo di Backward Chaining per FOL

![Algoritmo di Backward Chaining per FOL](immagini/algoritmo-bc-fol.png)

#### Esempio di KB del Backward Chaining

![Esempio di KB del Backward Chaining 1](immagini/esempio-kb-america-bc-1.png)

![Esempio di KB del Backward Chaining 2](immagini/esempio-kb-america-bc-2.png)

![Esempio di KB del Backward Chaining 3](immagini/esempio-kb-america-bc-3.png)

#### Proprietà del Backward Chaining per FOL

- Complessità in spazio lineare con la dimensione della prova
- Incompleta a causa di cicli infiniti, bisogna controllare il goal corrente rispetto ad ogni goal sulla pila che implementa la ricerca in profondità.
- Inefficiente a causa di sotto-goal ripetuti, sia di successo che di fallimento, dunque bisogna usare una cache che contiene i risultati già calcolati, dunque consumando più spazio.

Largamente utilizzato, senza i miglioramenti, per la programmazione logica (Prolog, ecc.).

### Conversione a CNF

Ognuno che ama tutti gli animali è amato da qualcuno:
$$
\forall x\ [\forall y\ (Animali(y)\implies Loves(x,y))]\implies [\exists y\ Loves(y,x)]
$$

1. Eliminare doppie e singole implicazioni
   $$
   \forall x\ [\neg\forall y (\neg Animal(y)\lor Loves(x,y))]\lor[\exists y Loves(y,x)]
   $$
1. Spostare $\neg$ all'interno:
   $$
   \forall x\ [\exists y\neg(\neg Animal(y)\lor Loves(x,y))]\lor[\exists y\ Loves(y,x)]\\
   \forall x\ [\exists y\neg\neg Animal(y)\land \neg Loves(x,y)]\lor[\exists y\ Loves(y,x)]\\
   \forall x\ [\exists y\ Animal(y)\land \neg Loves(x,y)]\lor[\exists y\ Loves(y,x)]\\
   $$
1. Standardizzare le variabili, ogni quantificatore deve usarne una differente
   $$
   \forall x\ [\exists y\ Animal(y)\land \neg Loves(x,y)]\lor[\exists z\ Loves(z,x)]
   $$
1. Skolemizzare: ogni variabile esistenziale è rimpiazzata da una funzione di Skolem applicata a tutte quelle variabili quantificate universalmente nel cui scope compare il quantificatore esistenziale
   $$
   \forall x\ [Animal(F(x))\land\neg Loves(x,F(x))]\lor Loves(G(x),x)
   $$
1. Rimuovere i quantificatori universali
   $$
   [Animal(F(x))\land\neg Loves(x,F(x))]\lor Loves(G(x),x)
   $$
1. Distribuire $\land$ su $\lor$
   $$
   [Animal(F(x))\lor Loves(G(x),x)]\land[\neg Loves(x,F(x))\lor Loves(G(x),x)]
   $$

### Strategie di risoluzione

- Unit Clause: si preferisce effettuare la risoluzione con una delle sentenze costituita da un singolo letterale (clausola unitaria);
- Unit Resolution: forma ristretta di risoluzione in cui ogni passo di risoluzione deve coinvolgere una clausola unitaria (incompleta in generale, completa per clausole di Horn);
- Set of Support: basata sull'insieme di supporto da cui si preleva una delle sentenza e dove si pone il risolvente. Esempio di insieme di supporto; il goal, e tutti i risolventi derivati da esso;
- Input Resolution: combina una sequenza in input (KB e Query) con il risolvente corrente. Tipica struttura a spina di pesce.
- Linear Resolution: come Input Resolution, ma ammette anche la combinazione del risolvente corrente con suoi avi;
- Subsumption: elimina tutte le sentenze che sono "subsumed", ovvero più specifiche di altre, ad esempio $P(A)$ e $P(A)\lor P(B)$ sono più specifiche di $P(x)$.

### Schema prova di completezza

![Schema prova di completezza](immagini/schema-prova-di-completezza.png)

## Incertezza

Presa l'azione $A_t$ "partire dall'aeroporto $t$ minuti prima del volo", l'azione $A_t$ mi permetterà di arrivare in tempo?

**Problemi**:

1. Osservabilità parziale: non abbiamo conoscenza completa ad esempio dello stato della strada, del piano di altri veicoli, ecc. ;
1. Sensori rumorosi: ad esempio i rapporti sul traffico, che non sono particolarmente precisi;
1. Incertezza nell'esito delle azioni: magari uno pneumatico forato, ecc. ;
1. Immensa complessità nel modellare e nel predire il traffico.

Un approccio puramente logico rischia di dire il falso o di condurre a conclusioni che sono troppo deboli per prendere decisioni:

- "$A_{25}$ mi fa arrivare in tempo", ma è basata su dati poco precisi;
- "$A_{1440}$ mi fa arrivare in tempo", ma passo la notte in aeroporto.

### Probabilità

Le asserzioni probabilistiche riassumono gli effetti di

- **Pigrizia**: fallimento nell'enumerare le eccezioni, qualifica, ecc. ;
- **Ignoranza**: mancanza di fatti rilevanti, condizioni iniziali, ecc.

#### Probabilità Soggettiva o Bayesiana

Le probabilità legano le proposizioni al proprio stato di conoscenza
$$
P(A_{25}\mid\text{nessun incidente riportato})=0.06
$$
Non indica alcuna *tendenza probabilistica* nella situazione corrente.

Le probabilità delle proposizioni cambiano con l'arrivo di nuova evidenza:
$$
P(A_{25}\mid\text{nessun incidente riportato}, \text{5 a.m.})=0.15
$$

#### Decidere nell'incertezza

Supponiamo di credere che:
$$
\begin{aligned}
   P(A_{25}\text{ mi fa arrivare in tempo}\mid\ldots)&=0.04\\
   P(A_{90}\text{ mi fa arrivare in tempo}\mid\ldots)&=0.70\\
   P(A_{120}\text{ mi fa arrivare in tempo}\mid\ldots)&=0.95\\
   P(A_{1440}\text{ mi fa arrivare in tempo}\mid\ldots)&=0.9999\\
\end{aligned}
$$
Quale azione scegliere?

Dipende dalle mie *preferenze*: pendere l'aereo vs. fare colazione in aeroporto, ecc.

**Teoria dell'utilità** è usata per rappresentare e inferire preferenze.

**Teoria delle decisioni** come unione della teoria dell'utilità e teoria delle probabilità.

### Probabilità a priori

Le probabilità **prior** o **probabilità incondizionate** di proposizioni

**Esempio**: $P(Cavità=vero)=0.1$ e $P(Tempo=sole)=0.72$ corrispondono a gradi di credenza sull'arrivo di nuova evidenza.

Una **distribuzione di probabilità** fornisce valori per tutti i possibili assegnamenti:

**Esempio**: $Tempo$ può assumere i valori $\langle sole,pioggia,nuvole,neve\rangle$ allora $\mathbf{P}(Tempo)=\langle 0.72,0.1,0.08,0.1\rangle$.
Le probabilità normalizzate sommano a 1.

Una **distribuzione di probabilità congiunta** per un insieme di variabili aleatorie fornisce la probabilità che ogni evento atomico su tali variabili.

**Esempio**: $\mathbf{P}(Tempo,Cavità)$ è una matrice $4\times 2$ di valori:

| $Tempo=$        | $sole$ | $pioggia$ | $nuvole$ | $neve$ |
| ---------------- | ------ | --------- | -------- | ------ |
| $Cavità=vero$    | 0.144  | 0.02      | 0.016    | 0.02   |
| $Cavità = falso$ | 0.576  | 0.08      | 0.064    | 0.08   |

Ogni domanda che concerne un dominio trova risposta nella distribuzione congiunta poiché ogni evento è la somma dei possibili eventi.

La dimensione dei dati è esponenziale nel numero di variabili, per rendere i problemi trattabili è spesso necessario fattorizzare l'insieme dei dati in oggetti più piccoli, che uniti approssimino il sistema iniziale.

### Probabilità condizionale

Una **probabilità a posteriori** o **condizionale** dipende da un insieme di probabilità.

**Esempio**:
$P(Cavità\mid MalDiDenti=0.8$ ovvero dato che $MalDiDenti$ è vero, ho probabilità $0.8$ di avere una cavità.

Se so di più, ad esempio che $Cavità$ è data, allora abbiamo $P(Cavità\mid MalDiDenti,Cavità)=1$

La credenza meno specifica rimane valida dopo che nuova evidenza arriva, ma non necessariamente rimane utile, come nel caso sopra.

Nuova credenza può essere irrilevante, permettendo semplificazioni.
Questo tipo di inferenza dipende dalla conoscenza del dominio ed è cruciale per semplificare i problemi.

**Esempio**:
$P(Cavità\mid MalDiDenti,VinceInter)=P(Cavità\mid MalDiDenti)=0.8$

#### Definizione di Probabilità condizionale

Definiamo la probabilità condizionale come
$$
P(a\mid b)=\frac{P(a\land b)}{P(b)}\text{ if }P(b)\not=0
$$

La **regola del prodotto** fornisce la definizione alternativa
$$
P(a\land b)=P(a\mid b)P(b)=P(b\mid a)P(a)
$$

Una versione generale vale sulle distribuzioni.

**Esempio**:
$\mathbf{P}(Tempo,Cavità)=\mathbf{P}(Tempo\mid Cavità)\mathbf{P}(Cavità)$ visto come un insieme $4\times 2$ di equazioni, non come una moltiplicazione di matrici.

La **chain rule** è derivata dall'applicazione ripetuta della regola del prodotto:
$$
\begin{aligned}
   \mathbf{P}(X_1,\ldots,X_n)
   &=\mathbf{P}(x_1,\ldots,x_{n-1})\mathbf{P}(X_n\mid X_1,\ldots,X_{n-1})\\
   &=\mathbf{P}(x_1,\ldots,x_{n-2})\mathbf{P}(X_{n-1}\mid X_1,\ldots,X_{n-2})\mathbf{P}(X_n\mid X_1,\ldots,X_{n-1})\\
   &=\ldots\\
   &=\prod_{i=1}^n\mathbf{P}(X_i\mid X_1\ldots,X_{i-1})
\end{aligned}
$$

### Inferenza tramite enumerazione

Si inizia con la distribuzione congiunta:

![Esempio distribuzione congiunta](immagini/esempio-distribuzione-congiunta.png)

Per ogni proposizione $\phi$, si sommano gli eventi atomici dove essa è vera.
Si trova la probabilità dell'evento come somma delle probabilità che lo rendono vero.
$$
\begin{aligned}
P(\phi)&=\sum_{\omega\mid\omega\vDash\phi}P(\omega)\\
P(toothache)&=0.108+0.012+0.016+0.064=0.2\\
P(toothache\lor cavity)&=0.108+0.012+0.016+0.064+0.072+0.008=0.28\\
P(\neg cavity\mid toothache)&=\frac{P(\neg cavity\land toothache)}{P(toothache)}=\frac{0.016+0.064}{0.108+0.012+0.016+0.064}=0.4
\end{aligned}
$$

Il denominatore può essere visto come una costante di normalizzazione $\alpha$.
Possiamo dunque ricavarlo alla fine, poiché sappiamo che le probabilità devono sommarsi a 1,questo ci permette di saltare dei calcoli.
$$
\begin{aligned}
\mathbf{P}(Cavity\mid toothache)
&=\alpha\mathbf{P}(Cavity,toothache)\\
&=\alpha[\mathbf{P}(Cavity,toothache,catch)+\mathbf{P}(Cavity,toothache,\neg catch)]\\
&=\alpha[\langle0.108,0.016\rangle+\langle0.012,0.064\rangle]\\
&=\alpha\langle0.12,0.08\rangle=\langle0.6,0.4\rangle
\end{aligned}
$$
L'idea generale è calcolare la distribuzione sulla variabile della query fissando le **variabili di evidenza** e sommando sulle **variabili nascoste**.

Nel caso sopra $Cavity$ è una **variabile di query**, $toothache$ è una **variabile in evidenza**, mentre $catch$ è una **variabile nascosta**.

Tipicamente siamo interessati alla distribuzione congiunta a posteriori delle **variabili di query** $\mathbf{Y}$ dati specifici valori $\mathbf{e}$ per le variabili di evidenza

Poniamo le *variabili nascoste* essere $\mathbf{H}=\mathbf{X}-\mathbf{Y}-\mathbf{E}$

Allora la somma desiderata di entrate congiunte è ottenuta sommando sulle variabili nascoste:
$$
\mathbf{P}(\mathbf{Y}\mid\mathbf{E}=\mathbf{e})=\alpha\mathbf{P}(\mathbf{Y},\mathbf{E}=\mathbf{e})=\alpha\sum_{\mathbf{h}\in\mathbf{H}}\mathbf{P}(\mathbf{Y},\mathbf{E}=\mathbf{e},\mathbf{H}=\mathbf{h})
$$

I termini nella sommatoria sono entrate congiunte perché $\mathbf{Y}$, $\mathbf{E}$ e $\mathbf{H}$ insieme esauriscono l'insieme delle variabili aleatorie $\mathbf{X}$.

**Problemi ovvi**:

- Complessità del caso pessimo in tempo $O(d^n)$ dove $d$ è l'arità più grande;
- Complessità in spazio $O(d^n)$ per memorizzare la distribuzione congiunta;
- Come stabilire  valori per $O(d^n)$ entrate?

### Indipendenza

$A$ e $B$ sono indipendenti se e solo se
$$
\mathbf{P}(A\mid B)=\mathbf{P}(A)\text{ or }\mathbf{P}(B\mid A)=\mathbf{P}(B)\text{ or }\mathbf{P}(A,B)=\mathbf{P}(A)\mathbf{P}(B)
$$

**Esempio**:
$\mathbf{P}(Toothache,Catch,Cavity,Weather)=\mathbf{P}(Toothache,Catch,Cavity)\mathbf{P}(Weather)$.
In questo modo 32 entrate vengono ridotte a 12.

L'indipendenza assoluta è potente, ma rara.
Nei problemi reali sono coinvolte centinaia di variabili, nessuna delle quali è indipendente.

### Indipendenza condizionale

$\mathbf{P}(Toothache,Cavity,Catch)$ ha $w^3-1=7$ entrate indipendenti.
Se si ha una cavità, la probabilità che la sonda la scopri non dipende dal fatto di avere il mal di denti, dunque $P(catch\mid toothache,cavity=)=P(catch\mid cavity)$.

La stessa indipendenza vale se non c'è la cavità, dunque $P(catch\mid toothache,\neg cavity)=P(catch\mid\neg cavity)$.

$Catch$ è **condizionalmente indipendente** da $Toothache$ dato $Cavity$, ovvero $\mathbf{P}(Catch\mid Toothache,Cavity)=\mathbf{P}(Catch\mid Cavity)$

Equivalentemente, possiamo affermare che
$$
\begin{aligned}
\mathbf{P}(Toothache\mid Catch,Cavity)&=\mathbf{P}(Toothache\mid Cavity)\\
\mathbf{P}(Toothache,Catch\mid Cavity)&=\mathbf{P}(Toothache\mid Cavity)\mathbf{P}(Catch\mid Cavity)
\end{aligned}
$$

Possiamo infine scrivere la distribuzione congiunta completa usando la chain rule:
$$
\begin{aligned}
   &\mathbf{P}(Toothache,Catch,Cavity)\\
   &=\mathbf{P}(Toothache\mid Catch,Cavity)\mathbf{P}(Catch,Cavity)\\
   &=\mathbf{P}(Toothache\mid Catch,Cavity)\mathbf{P}(Catch\mid Cavity)\mathbf{P}(Cavity)\\
   &=\mathbf{P}(Toothache\mid Cavity)\mathbf{P}(Catch\mid Cavity)\mathbf{P}(Cavity)\\
\end{aligned}
$$

Nel secondo passo abbiamo potuto rimuovere la dipendenza da $Catch$, dato che l'informazione è irrilevante.
Abbiamo, dunque, ottenuto $2+2+1=5$ numeri indipendenti

In molti casi, l'uso di indipendenza condizionale riduce la dimensione della rappresentazione della probabilità congiunta da essere esponenziale in $n$ a lineare in $n$.

L'indipendenza condizionale è la forma più basilare e robusta di conoscenza sugli ambienti incerti.

### Regola di Bayes

Indichiamo come **regola di Bayes** la formula derivata dalla regola del prodotto
$$
P(a\mid b)=\frac{P(b\mid a)P(a)}{P(b)}
$$
o in forma di distribuzione
$$
\mathbf{P}(Y\mid X)=\frac{\mathbf{P}(X\mid Y)\mathbf{P}(Y)}{\mathbf{P}(X)}=\alpha\mathbf{P}(X\mid Y)\mathbf{P}(Y)
$$

Questa formula è utile per ottenere probabilità diagnostica a partire da probabilità causale:
$$
P(Cause\mid Effect)=\frac{P(Effect\mid Cause)P(Cause)}{P(Effect)}
$$

#### Regola di Bayes e indipendenza condizionale

Prendiamo la distribuzione
$$
\begin{aligned}
&\mathbf{P}(Cavity\mid toothache\land catch)\\
&=\alpha\mathbf{P}(toothache\land catch\mid Cavity)\mathbf{P}(Cavity)\\
&=\alpha\mathbf{P}(toothache\mid Cavity)\mathbf{P}(catch\mid Cavity)\mathbf{P}(Cavity)
\end{aligned}
$$

Questo è un esempio di modello **Naive Bayes**
$$
\mathbf{P}(Cause,Effect_1,\ldots,Effect_n)=\mathbf{P}(Cause)\prod_i\mathbf{P}(Effect_i\mid Cause)
$$
Il numero totale di parametri è *lineare* in $n$

#### Esempio probabilistico del mondo de Wumpus

- $P_{ij}=vero$ se e solo se $[i,j]$ contiene una trappola
- $B_{ij}=vero$ se e solo se $[i,j]$ è ventilato

Includiamo solo $B_{1,1}$,$B_{1,2}$ e $B_{2,1}$ nel modello probabilistico.

La distribuzione congiunta completa è $\mathbf{P}(P_{1,1},\ldots,P_{4,4},B_{1,1},B_{1,2},B_{2,1})$

Applicando la regola del prodotto otteniamo $\mathbf{P}(B_{1,1},B_{1,2},B_{2,1}\mid P_{1,1},\ldots,P_{4,4})\mathbf{P}(P_{1,1},\ldots,P_{4,4})$, in questo modo otteniamo una probabilità condizionata della forma $P(Effect\mid Cause)$.

I primi termini sono 1 se le trappole adiacenti sono brezze, 0 altrimenti.
I secondi termini indicano la probabilità che sia presente una trappola, ma dato che queste sono posizionate a caso con probabilità fissata $0.2$ per quadrato, $\mathbf{P}(P_{1,1},\ldots,P_{4,4})=\prod_{i,j=1,1}^{4,4}\mathbf{P}(P_{i,j})=0.2^n\times 0.8^{16-n}$ per $n$ trappole.

Noi conosciamo i seguenti fatti:

- $b=\neg b_{1,1}\land b_{1,2} \land b_{2,1}$
- $known=\neg p_{1,1}\land\neg p_{1,2}\land\neg p_{2,1}$

La query è $\mathbf{P}(P_{1,3}\mid known,b)$

Definiamo $unknown=\{P_{i,j}\mid P_{i,j}\not=P_{1,3}\land P_{i,j}\not=known\}$

Per effettuare inferenza per enumerazione, abbiamo
$$
\mathbf{P}(P_{1,3}\mid known,b)=\alpha\sum_{unknown}\mathbf{P}(P_{1,3},unknown,known,b)
$$
Cresce esponenzialmente con il numero di quadrati.

**Usando l'indipendenza condizionale**

Le osservazioni sono condizionalmente indipendenti da altri quadrati nascosti dai i quadrati nascosti adiacenti.

![Esempio indipendenza condizionale mondo dei Wumpus](immagini/esempio-indipendenza-condizionale-wumpus.png)

Definiamo $Unknown=Fringe\bigcup Other$, dunque $\mathbf{P}(b\mid P_{i,3,Known,Unknown})=\mathbf{P}(b\mid P_{1,3},Known,Fringe)$

Poniamo la query in una forma dove si possa usare quanto sopra.
$$
\begin{aligned}
\mathbf{P_{1,3}\mid known,b}
&=\alpha\sum_{unknown}\mathbf{P}(P_{1,3},unknown,known,b)\\
&=\alpha\sum_{unknown}\mathbf{P}(b\mid P_{1,3},unknown,known)\mathbf{P}(P_{1,3},unknown,known)\\
&=\alpha\sum_{fringe}\sum_{other}\mathbf{P}(b\mid known,P_{1,3},fringe,other)\mathbf{P}(P_{1,3},known,fringe,other)\\
&=\alpha\sum_{fringe}\sum_{other}\mathbf{P}(b\mid known,P_{1,3},fringe)\mathbf{P}(P_{1,3},known,fringe,other)\\
&=\alpha\sum_{fringe}\mathbf{P}(b\mid known,P_{1,3},fringe)\sum_{other}\mathbf{P}(P_{1,3},known,fringe,other)\\
&=\alpha\sum_{fringe}\mathbf{P}(b\mid known,P_{1,3},fringe)\sum_{other}\mathbf{P}(P_{1,3})P(known)P(fringe)P(other)\\
&=\alpha P(known)\mathbf{P}(P_{1,3})\sum_{fringe}\mathbf{P}(b\mid known,P_{1,3},fringe)P(fringe)\sum_{other}P(other)\\
&=\alpha'\mathbf{P}(P_{1,3})\sum_{fringe}\mathbf{P}(b\mid known,P_{1,3},fringe)P(fringe)
\end{aligned}
$$
Troviamo così
$$
\begin{aligned}
   \mathbf{P}(P_{1,3}\mid known,b)&=\alpha'\langle 0.2(0.04+0.16+0.16),0.8(0.04+0.16)\rangle\\
   &\approx \langle0.31,0.69\rangle\\\\
   \mathbf{P}(P_{2,2}\mid known,b)&=\alpha'\langle0.2(0.04+0.16+0.16+0.64),0.8(0.4)\rangle\\
   &\approx\langle 0.86,0.14\rangle
\end{aligned}
$$

## Reti Bayesiane (Bayesian Networks)

Le reti Bayesiane sono una semplice notazione grafica per le asserzioni condizionalmente indipendenti e quindi per specifiche di distribuzioni condizionali complete.

**Sintassi**:

- Un insieme di nodi, uno per variabile;
- Un grafo diretto aciclico (link $\approx$ "influenza direttamente");
- Una distribuzione condizionale per ogni nodo dati i suoi genitori: $\mathbf{P}(X_i\mid Parents(X_i))$

Il caso più semplice è quello di una distribuzione condizionale rappresentata come una tabella della probabilità condizionale (CPT) data la distribuzione su $X_i$ per ogni combinazione di valori assunti dai genitori.

![Esempio reti Bayesiane](immagini/esempio-reti-bayesiane.png)

### Compattezza

una CPT per variabili booleane $X_i$ con $k$ genitori booleani ha 2^k$ righe per le combinazioni di valori dei genitori.

Ogni riga richiede un numero $p$ per $X_i=vero$ (il numero per $X_i=falso$ è $1-p$).

Se ogni variabile non ha più di $k$ genitori la rete completa richiede $O(n\cdot 2^k)$ numeri.

Ciò mostra che il modello cresce linearmente con $n$, al contrario della distribuzione congiunta completa che segue $O(2^n)$.
Per la rete precedente usiamo $1+1+4+2+2=10$ numeri, invece di $2^5-1=31$.

### Semantica globale

La semantica globale definisce la distribuzione congiunta completa come il prodotto delle distribuzioni condizionali locali:
$$
\mathbf{P}(X_1,\ldots,X_n)=\prod_{i=1}^n\mathbf{P}(X_i\mid Parents(X_i))
$$

**Esempio**:

![Esempio reti Bayesiane](immagini/esempio-piccolo-reti-bayesiane.png)

Dalla query $P(j\land m\land a\land\neg b\land\neg e)$ otteniamo $P(j\mid a)P(m\mid a)P(a\mid\neg b,\neg e)P(\neg b)P(\neg e)$

### Complessità dell'inferenza esatta

Reti **singolarmente connesse** (o **polytree**):

- Ogni coppia di nodi è connessa ad al più un cammino non diretto;
- Il costo in tempo e spazio dell'eliminazione di una variabile è $O(d^kn)$

Reti connesse più che singolarmente:

- Possibile ridurre 3SAT all'inferenza esatta, ma è NP-hard;
- Equivalente a modelli 3SAT con conteggio del numero di soluzioni ed è #P-completo

### Inferenza tramite simulazione stocastica

Idea base:

- Estrarre $N$ campioni da una distribuzione di campionamento $S$;
- Calcolare la probabilità a posteriori approssimata $\hat{P}$;
- Mostrare che converge alla vera probabilità $P$

Outline:

- Campionamento da una rete vuota;
- Rejection sampling: rigettare i campioni in disaccordo con l'evidenza;
- Likelihood weighting: usare l'evidenza per pesare i campioni;
- Markov chain Monte Carlo (MCMC): campiona in accordo ad un processo stocastico la cui distribuzione stazionaria è la vera probabilità.

## Apprendimento automatico

L'apprendimento automatico è utile nei casi in cui è difficile formalizzare il problema, ma è semplice fornire degli esempi, oppure quando c'è molto rumore nei dati.

Definiamo un algoritmo di apprendimento come un algoritmo che è capace di apprendere dei dati.

I tre ingredienti fondamentali per un algoritmo di apprendimento sono:

- Il compito, detto **task**;
- La misura di prestazione, detta **performance measure**;
- L'esperienza, detta **experience**.

### Task

Un task viene solitamente descritto in termini di come l'algoritmo di apprendimento automatico dovrebbe elaborare un *esempio*.

Ciascun esempio è rappresentato come una collezione di caratteristiche, dette **feature** che possono essere misurate.

La varietà dei task è molto grande, i seguenti sono solo alcuni esempi:

- Classificazione (con valori di caratteristiche mancanti);
- Regressione;
- Trascrizione;
- Traduzione automatica;
- Output strutturato;
- Rilevazione di anomalie;
- Sintesi e campionamento;
- Completamento di valori mancanti;
- Rimozione del rumore;
- Stima di densità o stima della funzione di massa di probabilità.

### Misura di prestazione

Quanto è buono un algoritmo di apprendimento?
Dobbiamo misurare le sue prestazioni, cioè quanto è accurata la funzione/modello da lui generato.

La misura di prestazione dipende dal task, ad esempio potremmo utilizzare l'accuratezza (*accuracy*) per la classificazione o il Mean Square Error (MSE) per la regressione.

### Esperienza

Il set dei dati da cui apprendere l'algoritmo può essere di vario genere:

- Che tipo di dati:
  - Caratteristiche a valori reali;
  - Caratteristiche a valori discreti;
  - Caratteristiche miste;
- Come otteniamo i dati:
  - Una volta per tutte (batch learning);
  - Acquisisci incrementalmente integrando con l'ambiente (online learning);
- Come possiamo usare i dati, ci sono diversi paradigmi di apprendimento.

### Principali paradigmi di apprendimento

#### Apprendimento Supervisionato

Dato un insieme di esempi pre-classificati $Tr=\{(x^{(i)},f(x^{(i)}))\}$, apprendere una descrizione generale che incapsula l'informazione contenuta negli esempi, regole valide su tutto il dominio in ingresso.

La descrizione ottenuta deve poter essere usata in modo predittivo, ovvero, dato un nuovo ingresso $\tilde{x}$, deve predire l'output associato a $f(\tilde{x})$.

Si assume che un esperto ci fornisca la supervisione, un oracolo, che ci da i valori della $f$ per le istanze $x$ dell'insieme di apprendimento.

#### Apprendimento Non Supervisionato

Dato un insieme di esempi $Tr=\{x^{(i)}\}$, estrarre regolarità e/o pattern validi su tutto il dominio in ingresso.
Non esiste un esperto che ci fornisca alcun aiuto.

Le tecniche più usate sono il **Clustering** e la Scoperta di Regole (**Discovery**).

#### Apprendimento Con Rinforzo

Sono dati:

- Agente, possibilmente intelligente, che può trovarsi in uno stato $s$ ed eseguire un'azione $a$ (all'interno delle possibili azioni nello stato corrente);
- un ambiente $e$, che applicando un'azione $a$ nello stato $s$ restituisce lo stato successivo e una ricompensa $r$, che può essere positiva, negativa o neutra.

Lo scopo dell'agente è quello di massimizzare una funzione delle ricompense.

### Spazio delle ipotesi

Lo spazio delle ipotesi $\mathcal{H}$

- Costituisce l'insieme delle funzioni che possono essere realizzate dal sistema di apprendimento;
- Si assume che contenga la funzione $h$ tale che $h$ rappresenti la funzione $f$ o che almeno una ipotesi $h\in\mathcal{H}$ sia simile a $f$ per **approssimazione**.
- Non può coincidere con l'insieme di tutte le funzioni possibili e la ricerca essere esaustiva, altrimenti l'apprendimento è inutile, non ci sarebbe **bias induttivo**.

L'algoritmo di ricerca nello Spazio delle Ipotesi è l'algoritmo di apprendimento

### Perceptron

**Input**: Insieme di apprendimento $Tr=\{\overset{\rightarrow}{x},t\}$, dove $t\in\{-1,+1\}$

**Algoritmo**:

1. Inizializza il vettore dei pesi $\overset{\rightarrow}{w}$ al vettore nullo
1. Ripeti
   1. Seleziona uno degli esempi di apprendimento $(\overset{\rightarrow}{x},t)$ a caso
   1. se $out=sign(\overset{\rightarrow}{w}\cdot\overset{\rightarrow}{x})\not=t$ allora $\overset{\rightarrow}{w}\leftarrow\overset{\rightarrow}{w}+(t-out)\overset{\rightarrow}{x}$

#### Limiti del Perceptron

Il Perceptron è in grado di apprendere solo funzioni lineari con dati linearmente separabili, ad esempio non è in grado di rappresentare la funzione booleana **XOR**, poiché tratta dati non linearmente separabili.

### Errore Ideale

Supponiamo che la funzione $f$ da apprendere sia una funzione booleana $f:X\rightarrow\{0,1\}(\{-,+\})$

L'errore ideale $error_\mathcal{D}(h)$ di un'ipotesi $h$ rispetto al concetto $f$ e la distribuzione di probabilità $\mathcal{D}$, probabilità di osservare in ingresso $x\in X$, è la probabilità che $h$ classifichi erroneamente un input selezionato a caso secondo $\mathcal{D}$: $error_\mathcal{D}(h)\equiv\underset{x\in D}{Pr}[f(x)\not=h(x)]$.

### Errore Empirico

Dato $Tr$ il training set, ,più ipotesi possono essere consistenti: $h_0$, $h_1$, ...

Definiamo l'Errore Empirico di un'ipotesi $h$ come $error_{Tr}(h)\equiv\#\{(x,f(x))\in Tr\mid f(x)\not=h(x)\}$, ovvero il numero di esempi che $h$ classifica erroneamente.

Un'ipotesi $h$ è sovra-specializzata (**overfit**) su $Tr$ se $\exists h'\in\mathcal{H}$ tale che $error_{Tr}(h)<error_{Tr}(h')$, ma $error_\mathcal{D}>error_\mathcal{D}(h')$.

Il Validation Set serve per cercare di selezionare l'ipotesi migliore, evitando overfit.

## Apprendimento Con rinforzo

Sono dati:

- Un agente che può trovarsi in uno stato $s$ ed eseguire un'azione $a$ tra quelle disponibili nello stato corrente;
- Un ambiente $e$, che applicando un'azione $a$ nello stato $s$ restituisce lo stato successivo e una ricompensa $r$ che può essere positiva, negativa o neutra.

Lo scopo dell'agente è quello di massimizzare la funzione delle ricompense.
$$
r_0+\gamma r_1+\gamma^2 r_2+\ldots\text{ dove }0\leq\gamma<1
$$

![Schema apprendimento con rinforzo](immagini/schema-apprendimento-rinforzo.png)

## Processi di Decisione di Markov

Si assume

- Insieme finito di stati $S$;
- Insieme di azioni $A$;
- Ad ogni istante di tempo $t$ l'agente osserva lo stato $s_t\in S$ e sceglie l'azione $a_t\in  A$;
- Poi riceve la ricompensa immediata $r_t$ e cambia lo stato in $s_{t+1}$;
- Assunzione di Markov: $s_{t+1}=\delta(s_t,a_t)$ e $r_t=r(s_t,a_t)$
  - cioè, $r_t$ e $s_{t+1}$ dipendono **solo** dallo stato ed azione correnti;
  - le funzioni $\delta$ e $r$ possono essere **deterministiche** o **non deterministiche**;
  - le funzioni $\delta$ e $r$ possono essere **conosciute** o **ignote**.

### La Funzione di Valutazione

Consideriamo il caso deterministico.
Per ogni possibile politica $\pi$ che l'agente può adottare, possiamo definire una funzione di valutazione sugli stati
$$
\begin{aligned}
V^\pi(s)&\equiv r_t+\gamma r_{t+1}+\gamma^2 r_{t+2}+\ldots \\
&\equiv\sum_{i=0}^\infty\gamma^ir_{t+i}
\end{aligned}
$$
dove $r_t,r_{t+1},\ldots$ sono generati seguendo la politica $\pi$ a partire dallo stato $s$.
Riformulato, il compito consiste nell'apprendere la politica ottima $\pi^*$ tale che
$$
\pi^*\equiv arg\text{ max}_\pi V^\pi(s),(\forall s)
$$

Possiamo tentare di far apprendere all'agente la funzione di valutazione $V^{\pi^*}$, riferita in seguito come $V^*$.

L'agente potrebbe effettuare una ricerca in avanti per scegliere la migliore azione a partire ad ogni stato $s$ perché
$$
\pi^*(s)=arg\text{ max}_a[r(s,a)+\gamma V^*(\delta(s,a))]
$$
**Problema**:
Funziona solo se l'agente conosce $\delta:S\times A\rightarrow S$ e $r:S\times A\rightarrow\mathfrak{R}$, ma se l'agente non ha questa conoscenza, come succede in molte applicazioni del mondo reale. non si può procedere in questo modo.

### La funzione $Q()$

**Soluzione**:
Definiamo una nuova funzione molto simile a $V^*$
$$
Q(s,a)\equiv r(s,a)+\gamma V^*(\delta(s,a))
$$
Se l'agente apprende $Q$, esso può scegliere l'azione ottima anche senza conoscere $\delta$.
$$
\pi^*(s)=arg\ max_a[r(s,a)+\gamma V^*(\delta(s,a))]\\
\pi^*(s)=arg\ max_a Q(s,a)
$$
Quindi, $Q$ è la funzione di valutazione che l'agente deve apprendere.

### Come apprendere $Q()$

Notare che $Q$ e $V^*$ sono intimamente correlate:
$$
V^*(s)=max_{a'}\ Q(s,a')
$$
Ciò permette di scrivere $Q$ **ricorsivamente** come
$$
\begin{aligned}
   Q(s_t,a_t)
   &=r(s_t,a_t)+\gamma\ V^*(\delta(s_t,a_t))\\
   &=r(s_t,a_t)+\gamma\ max_{a'}\ Q(s_{t+1},a')
\end{aligned}
$$
Poniamo $\hat{Q}$ essere la funzione corrente appresa dall'agente, che approssima $Q$.
Consideriamo la seguente regola di apprendimento:
$$
\hat{Q}(s,a)\leftarrow r+\gamma\ max_{a'}\hat{Q}(s',a')
$$
dove $s'$ è lo stato risultante dall'applicazione dell'azione $a$ allo stato $s$.

Possiamo definire l'algoritmo di $Q$-Learning come:

1. Per ogni $s,a$ inizializza la entry della tabella $\hat{Q}(s,a)\leftarrow 0$
1. Osserva lo stato corrente $s$
1. Esegui per sempre
   1. Seleziona un'azione $a$ ed eseguila
   1. Ricevi la ricompensa immediata $r$
   1. Osserva il nuovo stato $s'$
   1. Aggiorna la entry $\hat{Q}(s,a$ come segue
      $$
      \hat{Q}(s,a)\leftarrow r+\gamma\ max_{a'}\hat{Q}(s',a')
      $$
   1. $s\leftarrow s'$

La scelta dell'azione da eseguire sarà randomica nel caso di esplorazione, mentre sceglie l'$arg\ max_a\hat{Q}(s,a)$ quando invece si vuole sfruttare l'azione.

Da notare che se le ricompense sono non-negative, allora
$$
\forall s,a,n\ \hat{Q}_{n+1}(s,a)\geq\hat{Q}_n(s,a)\\
\text{e}\\
\forall s,a,n\ 0\leq\hat{Q}_{n}(s,a)\leq Q(s,a)\\
$$

### Convergenza di Q-learning

$\hat{Q}$ converge a $Q$

**Dimostrazione per il caso deterministico**:
Definiamo un intervallo pieno un intervallo durante il quale ogni $(s,a)$ è visitato.
Durante ogni intervallo pieno l'errore più grande nella tabella $\hat{Q}$ è ridotto di un fattore $\gamma$.

Infatti, sia $\hat{Q}_n$ la tabella dopo $n$ aggiornamenti e $\Delta_n$ l'errore massimo in $\hat{Q}_n$, cioè $\Delta_n=max_{s,a}|\hat{Q}_n(s,a)-Q(s,a)|$

Per ogni entry della tabella $\hat{Q}_n(s,a)$ aggiornata all'iterazione $n+1$, l'errore nella stima rivista $\hat{Q}_{n+1}(s,a)$ è:
$$
\begin{aligned}
   |\hat{Q}_{n+1}(s,a)-Q(s,a)|
   &=|(r+\gamma max_{a'}\ \hat{Q}_n(s',a'))|-(r+\gamma max_{a'}\ Q(s',a'))\\
   &=\gamma|max_{a'}\hat{Q}_n(s',a')-max_{a'}Q(s',a')|\\
   &\leq\gamma max_{a'}|\hat{Q}_n(s',a')-Q(s',a')|\\
   &\leq\gamma max_{s'',a'}|\hat{Q}_n(s'',a')-Q(s'',a')|\\
   &\leq\gamma\Delta_n
\end{aligned}
$$
Da notare che abbiamo utilizzato il risultato generale $|max_{a}f_1(a)-max_{a}f_2(a)\leq max_a|f_1(a)-f_2(a)|$.

**Dimostrazione caso non deterministico**:
Si ridefiniscono $V,Q$ considerando i valori attesi:
$$
\begin{aligned}
V^\pi(s)
&\equiv E[r_t+\gamma r_{t+1}]\\
&\equiv E[\sum_{i=0}^\infty\gamma^i r_{t+1}]\\
Q(s,a)&\equiv E[e(s,a)+\gamma V^*(\delta(s,a))]
\end{aligned}
$$
Per l'apprendimento, sostituire la regola di aggiornamento con
$$
\hat{Q}_n(s,a)\leftarrow(1-\alpha_n)\hat{Q}_{n-1}(s-a)+\alpha_n[r+\gamma max_{a'}\hat{Q}_{n-1}(s',a')]
$$
dove
$$
\alpha_n=\frac{1}{1+visite_n(s,a)}
$$

## Natural Language Processing

Il linguaggio umano è specificatamente costruito per trasmettere un significato da parte dell'oratore/scrittore.
Non sono solo segnali, è una comunicazione libera.
Si usa una codifica che viene facilmente appresa dai bambini e che cambia nel tempo.

Un linguaggio umano è per lo più un sistema di segnali simbolico/categoriale.

I simboli categorici di un linguaggio possono essere codificati in diversi modi: suoni, gesti, simboli scritti o immagini.

I simboli sono invarianti su codifiche diverse (stesso alfabeto, diversa lingua).

Il Natural Language Processing (NLP) ha come obiettivo che un agente comprenda un linguaggio naturale tramite e che comunichi con degli esseri umani.
Questo gli permetterebbe di eseguire task particolarmente utili, come la traduzione tra più linguaggi, la risposta di domande, il riassunto di testi, ecc.

Comprendere e rappresentare completamente il significato di un linguaggio è un obiettivo molto complesso, dato che il linguaggio umano è spesso ambiguo.

NLP viene diviso nei seguenti livelli di complessità:

- Analisi dell'input
  - OCR/Tokenization e Fonetica/Analisi della pronuncia
- Analisi morfologica
- Analisi sintattica
- Interpretazione semantica
- Costruzione di discorsi

### Knowledge acquisition

Per prima cosa ci concentriamo sull'aspetto di estrazione dell'informazione di NLP:

- Classificazione del testo;
- Information Retrieval;
- Information Extraction.

Un fattore comune per la soluzione di questi problemi è l'utilizzo di modelli li linguaggi, ad esempio modelli che permettono di predire la distribuzione di probabilità delle espressioni di un linguaggio.

I linguaggi umani non sono formali, non seguono una grammatica formale come quelle dei linguaggi di programmazione, ne hanno una semantica formale.
Ci possono essere frasi il cui significato cambia radicalmente con l'aggiunta di un segno di punteggiatura, una pausa, un'intonazione o lo scambio d'ordine di due parole.
I linguaggi umani non sono immobili, vengono aggiunte nuove espressioni ogni giorno, mentre altre diventano antiquate.

Per semplificare il modello ci chiediamo se una stringa di parole appartena o meno ad un insieme di un certo linguaggio, o più formalmente cerchiamo $P(S=words)$.

Possiamo riutilizzare la chain-rule per costruire modelli predittivi a $n$ parole detti $n$-gram.

### Modello $n$-gram

Un modello $n$-gram è un modello per linguaggi definito come la distribuzione di probabilità sulle sequenze di caratteri.
$$
\mathbf{P}(c_{1:n})\equiv\text{la probabilità di trovare una sequenza di $n$ caratteri, da $c_1$ a $c_n$}
$$

Una sequenza di caratteri di lunghezza $n$ è detta $n$-gram.
I casi speciali sono l'unigramma, per $n=1$, il bigramma per $n=2$ e il trigramma per $n=3$.

**Esempio**:
In un testo inglese potremmo trovare che $P("the")=0.027$, mentre $P("zgq")=0.000000002$.

Più in generale, possiamo creare $n$-gram usando parole invece che solo lettere.

Un modello $n$-gram è una catena di Markov di ordine $n-1$.
In una catena di Markov la probabilità di trovare un carattere $c_i$  dipende solo dalla probabilità dei caratteri immediatamente precedenti, dunque nel trigramma
$$
P(c_i\mid c_{1:i-1})=P(c_i\mid c_{i-2;i-1})
$$
In generale possiamo definire un trigramma per fattorizzazione tramite la chain-rule e usando l'assunzione di Markov:
$$
P(c_{1:n})=\prod_{i=1}^nP(c_i\mid c_{1:i-1})=\prod_{i=1}^nP(c_i\mid c_{i-2:i-1})
$$

#### Usi del modello $n$-gram

Identificazione dei linguaggi: si può utilizzare per identificare in quale linguaggio è scritto un testo, semplicemente controllando la probabilità di trovare la sequenza data di caratteri/parole
$$
\mathbf{P}(Testo\mid Lingua)
$$
Selezioniamo la lingua più probabile dato il testo nel seguente modo:
$$
\begin{aligned}
l^*
&=arg\ max_l\ P(l\mid c:{i:n})\\
&=arg\ max_l\ P(l)P(x_{1:n}\mid l)\\
&=arg\ max_l\ P(l)\prod_{i=1}^nP(c_i\mid c_{i-2:i-1},l)\\
\end{aligned}
$$
Con un trigramma possiamo trovare la probabilità di un linguaggio dato un testo ma come otteniamo la probabilità a priori $P(l)$?
Possiamo stimare questi valori, ad esempio facendo una ricerca su testo casuale preso da Internet.
La scelta di questi prior non è critica, poiché il trigramma sceglie un linguaggio che ha un ordine di magnitudo più grande rispetto agli altri.

Altre applicazioni tipiche sono:

- Correzione del testo;
- Classificazione del genere di un testo;
- Riconoscimento di nome-entità.

#### Problemi del modello $n$-gram

Il principale problema degli $n$-gram è che non generalizzano bene: il corpus permette di trovare solo una stima della vera distribuzione di probabilità e se cerco di classificare testo mai visto, otterrò risultati completamente falsati.

#### Lisciare i modelli $n$-gram

Lo **smoothing** (lisciamento) è il processo di aggiustare la probabilità delle entry con frequenza molto bassa ad una probabilità non nulla piccola.

L'approccio più semplice è utilizzare il **Laplace smoothing** (o add-one smoothing), che data una variabile $X$ che non è stata mai registrata in $n$ osservazioni, allora assegniamo $P(X=true)=1/(n+2)$, assumendo che con altri due esempi, uno vero ed uno falso, avremmo trovato un osservazione.

Un approccio migliore è il **backoff model**.
In questo caso, inizialmente stimiamo i conteggi dell'$n$-gram.
Se una sequenza ha probabilità bassa o zero, proviamo ne calcoliamo la probabilità con un $n$-gram di un livello inferiore, $n-1$.
Il modello finale usa una interpolazione lineare che combina un trigramma, un bigramma e un unigramma con dei pesi:
$$
\hat{P}(c_i\mid c_{i-2:i-1})=\lambda_3P(c_i\mid c_{i-2:i-1})+\lambda_2P(c_i\mid c_i-1)+\lambda_1P(cI)
$$
con vincolo $\lambda_3+\lambda_2+\lambda_1=1$.

#### Valutazione del modello $n$-gram

Possiamo utilizzare cross-validation con metriche specifiche per il task, come l'accuracy sulla classificazione.

In generale una metrica che non dipende dal task e che funziona bene con gli $n$-gram è la **perplessità** (perplexity), definita come il reciproco della probabilità normalizzata dalla lunghezza della sequenza:
$$
Perplexity(c_{1:n})=P(c_{1:n})^{-\frac{1}{n}}
$$
Più la perplessità è bassa, migliore è il modello; questo perché un modello con bassa perplessità restituisce, in media, la migliore risposta quando cerchiamo di predire l'$n$-esimo elemento di una sequenza.
Minimizzando la perplessità stiamo massimizzando la probabilità delle sequenze corrette.

#### $n$-gram e chain rule

Cerchiamo di calcolare la probabilità di una sequenza di parole $P(W)=P(w_1,\ldots,w_n)$.
Un modello di un linguaggio calcola $P(W)$ o $P(w_n\mid w_1,\ldots,w_{n-1})$.

La chain rule applicata alla probabilità congiunta delle parole diventa $P(w_1w_2\ldots w_n)=\prod_iP(w_i\mid w_1\ldots w_{i-1})$, ad esempio
$$
\begin{aligned}
&P(\text{"its water is so transparent that"})=\\
&P(\text{"its"})P(\text{"water"}\mid\text{"its"})P(\text{"is"}\mid\text{"its water"})\\
&P(\text{"so"}\mid\text{"its water is"})P("transparent"\mid\text{"its water is so"})\\
&P(\text{"that"}\mid\text{"its water is so transparent"})
\end{aligned}
$$
La stima più corretta sarebbe
$$
\begin{aligned}
&P(\text{"the"}\mid\text{"its water is so transparent that"})=\\
&\frac{Count(\text{"its water is so transparent that the"})}{Count(\text{"its water is so transparent that"})}
\end{aligned}
$$
ma ci sono troppe parole, non avremo mai abbastanza dati per costruire un buon modello.

Semplifichiamo le assunzioni come nell'assunzione di Markov con $P(\text{"the"}\mid\text{"its water is so transparent that"})\approx P(\text{"the"}\mid\text{"that"})$ oppure $P(\text{"the"}\mid\text{"its water is so transparent that"})\approx P(\text{"the"}\mid\text{"transparent that"})$

La stima della Maximum Likelihood è
$$
P(w_i\mid w_{i-1})=\frac{Count(w_i,w_{i-1})}{Count(w_{i-1})}
$$

### Classificazione del testo

Il task è, dato un testo di qualche tipo e un set di classi, comprendere a quale classe il testo appartiene.
Questo tipo di classificazione può essere utile per identificare linguaggi, classificare generi di testo, fare analisi dei sentimenti, individuare spam, ecc.

La classificazione del testo avviene con due metodi:

- Classificazione con regole scritte a mano da un esperto;
- Utilizzo di modelli di linguaggi e machine learning.

Nel caso basato su regole, definiamo un insieme di regole basato su combinazioni di parole o altre feature.
Se le regole sono scelte bene, l'accuratezza può essere alta, ma scriverle può essere oneroso e non sempre possibile.

Se invece usiamo un modello di un linguaggio, possiamo semplicemente usare un classificatore Naive Bayes, utilizzando i valori di probabilità trovati.
Troviamo così l'ipotesi **maximum a posteriori**
$$
c_{MAP}=arg\ max_{c\in C}P(c\mid d)=arg\ max_{c\in C}\frac{P(d\mid c)P(c)}{P(d)}\cong arg\ max_{c\in C}P(d\mid c)P(c)
$$
Supponiamo che $d$ sia rappresentabile in $n$ feature $x_i$, allora la precedente equazione può essere riscritta come
$$
C_{MAP}=arg\ max_{c\in C}P(d\mid c)P(c)\cong arg\ max_{c\in C}P(x_1,\ldots,x_n\mid c)P(c)
$$
dove $P(d\mid c)$ è la componente legata alla **likelihood** e $P(c)$ è la probabilità prior.

Possiamo semplificare le assunzioni considerando che le feature del modello siano condizionalmente indipendenti, ovvero che $P(x_1,\ldots,x_n\mid c)=P(x_1\mid c)\ldots P(x_n\mid c)$.
Possiamo, così, costruire un **Classificatore Naive Bayes Multinomiale**
$$
c_{MAP}=arg\ max_{c\in C}P(x_1,\ldots,x_n\mid c)P(c)\\
c_{NB}=arg\ max_{x\in C}P(c)\prod_{x\in X}P(x\mid c)
$$

### Bag of words

Se non consideriamo importante l'ordine delle parole possiamo costruire modelli usando insiemi di parole.

Scriviamo in un vettore, per ogni parola, la frequenza di occorrenza di ciascuna parola.
Questi vettori sono grandi e sparsi.

È ugualmente possibile utilizzare vettori binari  per indicare la presenza di una parola.
Questi sono facilmente generabili tramite **one-hot encoding** di una bag of words.
In particolare è facile calcolare la similarità tra due vettori binari con un semplice prodotto vettoriale.

### Parole come simboli discreti

Il modo più comune di pensare al significato linguistico è la relazione tra un simbolo e un concetto, detto denotazione.
Per rappresentare la conoscenza utilizziamo tassonomie per costruire vettori di parole più significativi, detti WordNet.

Con questo metodo possiamo associare sinonimi, modi di dire, ecc. ma troviamo anche legami che non hanno un significato sensato.
Ad esempio l'aggettivo "proficient" è sinonimo di "good", ma non possono essere usati sempre intercambiabilmente.
Inoltre, costruire WordNet non è facile e mantenerle aggiornate è oneroso.

### Rappresentare parole tramite il loro contesto

L'idea è che il significato di una parola dipende da quelle ad essa vicine.
Possiamo costruire dei vettori densi che contengano la probabilità di trovare ciascuna altra parola in un intorno $n$ di parole vicine.

Word2Vec p una rete neurale **auto-encoder** che implementa questa idea di contesti per apprendere parole.
In particolare, nel modello allenato, troveremo che due parole utilizzate in modo simile saranno rappresentate da un vettore di pesi simile.

#### Vector space model

Un modello a spazio vettoriale (**vector model**) è un modello in cui un testo è rappresentato da un vettore di feature dei termini.

Funziona in modo simile alla Bag of Words, ma l'enfasi è sull'interpretazione geometrica

I documenti sono rappresentati come $\mathbf{d}_j=(s_{1,j},\ldots,w_{v,j})$

Preso un documento query $\mathbf{q}=(q_{q,j},\ldots,q_{v,j})$, calcoliamo la similarità coseno (**cosine similarity**) tra $\mathbf{q}$ e $\mathbf{d}_2$ calcolando
$$
cos(\theta)=\frac{\mathbf{d}_2\cdot q}{\parallel\mathbf{d}_2\parallel\cdot\parallel\mathbf{q}\parallel}
$$
Normalizziamo sulla lunghezza, dividendo ciascuno dei suoi componenti per la sua lunghezza
$$
\parallel\mathbf{d}_2\parallel=\sqrt{\sum_i w_{i,2}^2}=1
$$
dunque
$$
cos(\theta)=\frac{\mathbf{d}_2\cdot\mathbf{q}}{1\cdot 1}
$$

### Information Retrieval

Il **reperimento di informazione** è il task di trovare documenti di natura non strutturata, come del testo, che soddisfa un bisogno di informazione all'interno di una collezione di grande dimensione.

Un sistema IR può essere caratterizzato da:

- Il contenuto dei documenti;
- Le query poste in un linguaggio di query, magari usando operatori booleani e combinazioni;
- Un insieme di risultati, ad esempio un sottoinsieme dei documenti, giudicati rilevanti alla query;
- Una presentazione dei risultati, ad esempio una lista ordinata.

Una **funzione di score** prende un documento e una query, valuta il documento e restituisce uno score numerico; il documento più rilevante ha lo score più alto.

Un esempio è **BM25** che sfrutta tre fattori:

- La frequenza con cui il termine della query appare nel documento;
- L'**Inverse Document Frequency** (IDF) del termine;
- La lunghezza del documento.

Assumiamo di aver creato un indice degli $n$ documenti.
Possiamo calcolare $TF(q_i,d_j)$ and esempio contando il numero di vole in cui la parola $q_i$
appare nel documento $d_j$.
Assumiamo che la tabella della frequenza dei documenti conti $DF(q_i)$.

$$
BM25(d_j,q_{1:n})=\sum_{i=1}^nIDF(q_i)\cdot\frac{TF(q_i,d_j)\cdot(k+1)}{TF(q_i,d_j)+k\cdot(1-b+b\cdot\frac{|d_j|}{L})}
$$
$$
IDF(q_i)=log\frac{n-DF(q_i)+0.5}{DF(q_i)+0.5}
$$

#### TD-IDF weighting

TF-IDF può essere utilizzato come uno schema di pesi (smoothing) nel classico vector space model.

Sapendo che i documenti sono rappresentati come
$$
\mathbf{d}_j=(w_{1,j},\ldots,w_{v,j})
$$

Invece di usare i semplici termini pesati, ora abbiamo
$$
w_{t,j}=tf_{t,j}\cdot log\frac{|D|}{\{d'\in D\mid t\in d'\}}
$$
dove $tf_{t,j}$ è il termine frequenza di $t$ nel documento $j$ (parametro locale) e $log\frac{|D|}{\{d'\in D\mid t\in d'\}}$ è l'inverse document frequency (parametro globale).

Quanti termini frequenti e non frequenti dovremmo aspettarci da una collezione di documenti?
Possiamo stimarla con la **legge di Zipf**: dato un corpus, la frequenza di ogni parola p inversamente proporzionale al suo rank della tabella delle frequenze.

![Legge di Zipf](immagini/legge-di-zipf.png)

Riassumendo:

- Rappresentiamo la query come un vettore TF-IDF pesato;
- Rappresentiamo ogni documento come un vettore TF-IDF pesato;
- Calcoliamo la similarità coseno tra il vettore query e ciascun vettore documento;
- Ordinare i documenti per similarità rispetto la query;
- Restituire i primi $k$ risultati all'utente.

#### Valutazione del modello

Precision e Recall sono metriche tradizionali utilizzate per valutare le performance dei sistemi di reperimento di informazione:

- Precision: $\frac{TP}{TP+FP}$;
- Recall = $\frac{TP}{TP+FN}$;
- F1 = $2\frac{Prec\cdot Rec}{Prec+Rec}$
- Accuracy = $\frac{TP+TN}{TP+FP+TN+FN}$

In caso di dataset molto grandi recal diventa inutile, perciò si devono utilizzare metriche diverse.

**Precision@k**: calcola la precisione prendendo in considerazione soli i primi $k$ risultati.
Questo è particolarmente utile quando vogliamo misurare la qualità di un servizio di ricerca web accessibile agli utenti.

**Average Precision**: è ottenuto come la somma degli score di precisione in media su tutti i valori di recall
$$
AP=\frac{\sum_{k=1}^n(Prec(k)\cdot Rel(k))}{\#relevant\ documents}
$$
Usando **Mean AP** (MAP), ad esempio la media delle medie degli score di precisione per ogni query/classe.

#### Question Answering

La risposta a domande è per l'Information Retrieval un task tipico, che si risolve trovando documenti rilevanti alla query/domanda.
Quando si passa a bot che devono comprendere e rispondere a domande complesse, è necessario che essi comprendano correttamente il linguaggio utilizzato, inoltre ci si aspetta che rispondano in un linguaggio naturale.

### Information Extraction

L'Estrazione di Informazione è il processo di acquisizione di conoscenza tramite la lettura di testo e il controllo di occorrenza di particolari classi di oggetti e di relazioni tra oggetti.

Gli approcci tipici per Information Extraction sono:

- Un automa a stati finiti per Information Extraction;
- Modelli probabilistici per Information Extraction;

All'aumentare della generalità del dominio, sono necessari modelli linguistici e tecniche di apprendimento più complessi.

### Agenti basati su Information Retrieval

Idea principale:

- Ottenere un gran numero di conversazioni umano-umano o umano-macchina;
- Costruire un agente (chatbot) con la seguente architettura:
  - Al turno dell'utente viene definita la query $\mathbf{q}$;
  - L'agente crea un modello TF-IDF basato sul modello del corpus $C$;
  - Per ogni contenuto nel corpus, confronta $\mathbf{q}$ con $\mathbf{c}$
  - Restituisce la risposta legata al testo nel corpus che più assomiglia a quello della query
    $$
    \mathbf{r}=response\left(arg\ max_{\mathbf{t}\in C}\frac{\mathbf{q}\cdot\mathbf{t}}{\parallel\mathbf{q}\parallel\cdot\parallel\mathbf{t}\parallel}\right)
    $$

#### Agenti Frame-Based

Gli agenti Fame-Based o Task-Based usano la rappresentazione della conoscenza che hanno sul dominio, l'**ontologia**, per rappresentare le interazioni degli utenti.
Ciascun **frame** contiene un insieme di slot, ciascuno dei quali ha un valore informativo legato, che può essere ottenuto con una domanda all'utente.

**Esempio**:
Prendiamo il caso di un viaggio in aereo.
L'agente avrà bisogni di alcuni informazioni, come l'aeroporto di partenza, quello di destinazione, la data di partenza, la data di un eventuale ritorno, il nome della compagnia aerea, ecc.

Il metodo standard per ottenere queste informazioni è tramite un agente a stati finiti.
Ad ogni stato l'agente farà una domanda che può avere due o più possibili risposte.
Quello che l'utente risponde viene usato per estrapolare l'informazione necessaria a completare gli slot del frame che sono ancora vuoti.
Questo tipo di automi sono detti **Finite State Dialog Manager**.

![Agente RI a stati](immagini/agente-RI-a-stati.png)

Un sistema a stati in genere guida fortemente la conversazione.
Se le risposte date dall'utente sono mal interpretate, il sistema non funziona particolarmente bene.
Il passo successivo è costruire agenti che chiedono domande più generiche e che riempiono tutti gli slot con le risposte specificate dall'utente.
Solo se mancano ulteriori dati, allora usano domande specifiche.

Per raggiungere questi risultati si applicano inizialmente regole scritte a mano, per poi passare a metodi basati su machine learning.
In genere i modelli usati sono classificatori Naive Bayes, Reti Neurali, ecc. che mappano sequenze di parole a funzioni di riempimento dei frame.

## Computer Vision

Il goal principale della Computer Vision è la capacità di analizzare e comprendere il contenuto di un'immagine, identificarne gli oggetti o catalogarli.

Un'immagine contiene informazione semantica, come le classi degli oggetti contenuti o i tag legati ad una scena, informazioni geometriche e relazioni spaziali tra gli elementi contenuti.

La pipeline per il riconoscimento di un oggetto è la seguente:

- Input;
- Riconoscere la relazione tra le immagini;
  - Photo stitching
  - 3D urban modeling
- Riconoscimento delle strutture e del movimento tra i frame;
- Matching tramite mappe di profondità;
- Ricostruzione di un modello 3D di una struttura

### Sistemi di camere

Un sistema di camere formano la vista sull'ambiente e stabiliscono una mappa dal mondo tridimensionale al mondo bidimensionale, delle immagini.
Quando si vuole riconoscere un ambiente da una foto è necessario stimare i parametri della camera, ad esempio la sua posizione, la lunghezza focale, ecc.
Successivamente si cerca di stimare alcune delle proprietà del mondo dall'immagine.

### Image Formation

Proviamo a definire una camera.
Consideriamo una barriera con un foro molto piccolo nel centro; da una parte della barriera c'è l'oggetto che vogliamo fotografare, dall'altra un film impressionabile.

![Schema di una camera](immagini/schema-camera.png)

Questo modello è detto **Pinhole Camera**, camera oscura; il punto singolo al centro è detto apertura, centro di proiezione, centro ottico, punto di fuoco o centro della camera.
Questo tipo di camera è stata inventata intorno al 16° secolo DC.

Il modello a camera oscura descrive la geometria di una scena di un'immagine.
Utilizziamo un sistema di coordinate tridimensionale con origine nel centro ottico.
Un punto nella scena $P$ reale con coordinate $X,Y,Z$ viene proiettato nel punto $P'$ nel piano dell'immagine, con coordinate $x,y,z$.

![Modello di proiezione di una camera](immagini/modello-proiezione-camera.png)

Se $f$ p la distanza dall'origine al piano dell'immagine, allora per triangoli similari possiamo derivare le seguenti equazioni:
$$
\frac{-x}{f}=\frac{X}{Z},\frac{-y}{f}=\frac{Y}{Z}\implies x=\frac{-fX}{Z},y=\frac{-fY}{Z}
$$
Queste equazioni definiscono il processo di formazione dell'immagine noto come proiezione prospettica.

#### Notazione del sistema di assi

- L'asse principale, o asse ottico, è quello che punta nella direzione opposta del piano di proiezione;
- Il piano principale è quello su cui si trovano gli assi X e Y;
- Il piano dell'immagine è parallelo a X e Y e interseca l'asse Z alle coordinate $-f$ (con $f>0$).

Abbiamo definito un sistema di coordinate 3D con origine $\mathbf{O}$ e i tre assi $X1,X2,X3$.
Un punto $\mathbf{R}$, detto punto principale, all'intersezione tra l'asse ottico e il piano dell'immagine, ovvero il centro della proiezione.
Un punto $\mathbf{P}$ da qualche parte nel mondo alle coordinate $x_1,x_2,x_3$ relative agli assi $X1,X2,X3$.
La *linea di proiezione* dal punto $\mathbf{P}$ e passante per l'origine $\mathbf{O}$ si interseca con il piano di proiezione nel punto $\mathbf{Q}$
C'è, inoltre, un sistema di coordinate 2D con origine $\mathbf{R}$ e assi $Y1,Y2$, dove le coordinate del punto $\mathbf{Q}$ relative a questo sistema sono $y_1,y_2$.

![Schema assi camera](immagini/schema-assi-camera.png)

Come mostrato precedentemente per triangoli simili, possiamo trovare che il rapporto tra gli assi $y_1$ e $x_1$ è fisso, lo stesso vale per $y_2$ e $x_2$
$$
\frac{-y_1}{f}=\frac{x_1}{x_3}\implies y_1=-\frac{fx_1}{x_3}\\
\frac{-y_2}{f}=\frac{x_2}{x_3}\implies y_2=-\frac{fx_2}{x_3}\\
$$
Questa relazione può essere riassunta come
$$
\binom{y_1}{y_2}=-\frac{f}{x_3}\binom{x_1}{x_2}
$$

#### Punto di fuga (Vanishing point)

Considerando una proiezione prospettica, gli oggetti distanti appaiono più piccoli.
Un importante risultato di questo effetto è che linee parallele convergono in un punto all'orizzonte.
Tale punto è detto **punto di fuga** (vanishing point)

![Schema punto di fuga](immagini/schema-punto-di-fuga.png)

Possiamo trovare due tipi di punti di fuga:

- Punti di fuga singoli
  Accadono quando il piano di proiezione è parallelo a due coordinate del mondo reale.
  Il risultato è che le linee parallele agli altri assi non formano punti di fuga;
- Punti di fuga doppi
  Accadono quando il piano di proiezione interseca due coordinate del mondo reale.
  Il risultato è che le linee parallele a questi piani si incontreranno in due punti di fuga nel piano di proiezione

#### Svantaggi della camera oscura

- Il centro di proiezione deve essere piccolo affinché l'immagine sia a fuoco;
- Se centro di proiezione è piccolo si registrano meno fotoni e l'immagine è più scura;
- Per raccogliere più luce devo mantenere il foro aperto per più tempo, ma se il mondo si muove, l'immagine verrà sfocata (**motion blur**).

#### Dalla camera oscura alla camera a lenti

Consideriamo una camera oscura con più di un punto.
Il piano di proiezione conterrà un numero di immagini proiettate uguale al numero di punti di proiezione, ciascuno leggermente spostato rispetto agli altri, producendo un'immagine mal formata (fuzzy).

Considerando la possibilità di concentrare queste immagini in un unico punto, rimuovendo lo scostamento tra le immagini proiettate, questo ci permetterebbe di concentrare una maggiore quantità di luce, migliorando le prestazioni della camera.

A seconda della posizione in cui si trova la lente, la proiezione verrà più o meno concentrata correttamente sul piano di proiezione.
Questa proprietà viene controllata dal **punto di fuoco** (depth of field).

![Schema punto di fuoco](immagini/schema-punto-di-fuoco.png)

Nelle camere le lenti si muovono per mantenere l'immagine a fuoco, mentre negli occhi umani la lente cambia forma tramite dei muscoli specializzati.

### Visione Binoculare

Gli esseri umani sono capaci di percepire la profondità e altre informazioni dello spazio 3D tramite *stereopia*.
Gli occhi si trovano in posizioni laterali diverse, perciò riportano due immagini leggermente diverse.
Le differenze vengono processate dalla corteccia visiva, che restituisce una mappa della profondità.

La percezione della profondità deriva dalla diversità della proiezione di un certo punto tra le due immagini.

#### Geometria Epipolare

La geometria epipolare è la geometria della visione stereoscopica:

- Due camere osservano una scena tridimensionale da due posizioni, questo restituisce un numero di relazioni geometriche tra i punti tridimensionali e le loro proiezioni;
- Entrambe le camere sono approssimabili con il modello della camera oscura.

![Epipolar geometry](immagini/epipolar-geometry.png)

La figura mostra due camere che guardano il punto $P$.
$\mathbf{O}$ e $\mathbf{O'}$ sono i centri focali delle due camere e $\mathbf{p}$ e $\mathbf{p'}$ sono le proiezioni del punto $\mathbf{P}$ sul rispettivo piano di immagine.

Poiché i centri ottici delle due camere sono distinti, è possibile proiettare l'uno sul piano di proiezione dell'altro. I punti sulla proiezione $\mathbf{e,e'}$ prendono il nome di **epipoli** o **punti epipolari**.

Le rette passanti per i punti $\mathbf{e,p}$ e $\mathbf{e',p'}$ sono chiamate rette epipolari.

I punti $\mathbf{P,O,O'}$ formano un piano detto piano epipolare.

La retta passante per $\mathbf{O,P}$ è vista come come un punto dalla camera di sinistra e come un segmento tra $\mathbf{e',p'}$ dalla camera di destra.

Se la posizione relativa tra le due camere è nota, possiamo fare due importanti osservazioni:

- **vincolo epipolare**:
  La proiezione di $\mathbf{P}$ sul piano di destra, ovvero il punto $\mathbf{p'}$ deve essere contenuto nella retta epipolare passante per $\mathbf{e',p'}$, in particolare deve trovarsi tra i due punti.
  Questo permette di controllare se due punti sulla retta passante per $\mathbf{O,P}$ corrispondono allo stesso punto tridimensionale.
- **Triangolazione**:
  Se i punti $\mathbf{p}$ e $\mathbf{p'}$ sono noti, le loro rette di proiezione sono note e devono intersecarsi esattamente in $\mathbf{P}$.

#### Stereo matching

Eseguire lo stereo matching equivale a risolvere il problema di corrispondenza

![Stereo matching](immagini/stereo-matching.png)

##### Basic Stereo Matching

L'algoritmo più semplice per lo stereo matching controlla pixel per pixel la prima immagine e cerca corrispondenti rette epipolari sulla seconda immagine.
Successivamente si esaminano tutti i pixel sulla retta epipolare e si trovano i match migliori.
infine, triangola i match trovati e ottiene informazioni sulla profondità.
Una volta trovata almeno una retta epipolare nella seconda immagine è possibile rettificarla.
A questo punto si esegue una ricerca completa su tutti i punti, calcolando la disparità tra le posizioni trovate.

L'assunzione è che la maggior parte dei punti della scena sono visibili in entrambe le immagini e che le immagini rappresentino regioni simili.
Questa assunzione regge se la distanza tra i punti e la camera è più grande rispetto alla distanza tra le camere.

#### Il problema della corrispondenza

Il problema della corrispondenza è un problema di ricerca:

- Dato un elemento nell'immagine di sinistra, cerca un elemento corrispondente nell'immagine di destra;
- Tipicamente abbiamo bisogno di vincoli geometrici per ridurre lo spazio di ricerca;
- Dobbiamo scegliere gli elementi da matchare e una misura di similarità per compararli, ad esempio basata sulla correlazione o sulla somma quadratica delle distanze.
- Gli elementi da matchare sono frammenti di immagini di dimensione fissata.

![Image patch](immagini/image-patch.png)

Per trovare una corrispondenza utilizziamo una retta di scansione (*scanline*) e misuriamo la similarità dei contenuti per ogni patch su quella retta, calcolandone la similarità tramite la metrica definita sopra.

![Scanline example](immagini/scanline-example.png)

### Informazione semantica

La visione può essere una fonte di informazione semantica.

![Semantic information](immagini/semantic-information.png)

Le informazioni che possono essere estratte sono diverse:

- Che oggetti sono presenti nell'immagine;
- Dove sono gli oggetti nell'immagine.

#### Classificazione delle immagini

L'approccio data-driven consiste in:

- Collezionare un dataset di immagini e tag;
- Usare ML per allenare un classificatore;
- Valutare il classificatore su nuove immagini.

I risultati ottenuti da questi classificatori sono migliorati lentamente nel tempo finché nel 2015 vengono utilizzati molti più layer, con l'origine del **deep-learning**, raggiungendo accuracy addirittura migliori di quelle umane.

La pipeline tipica per identificare feature è la seguente:

- Input;
- Feature extraction;
- Codebook formation;
- Feature quantization (pooling);
- Feature

L'idea è di sfruttare un **Convolutional Neural Network** (ConvNet) identificare feature da diversi layer di astrazione sui dati iniziali.
Questo tipo di apprendimento è chiamato **Representation Learning**.

Nelle ConvNet la rete è composta da $n$ layer, di cui il primo è detto *layer di input* e l'ultimo è detto *layer di output*.

Le reti convoluzionali sfruttano una sequenza di layer dove ogni layer trasforma i suoi valori di output in valori di input per lo strato successivo tramite una funzione derivabile.

Ci sono tre tipi principali di layer:

- Layer convoluzionali;
- Layer di pooling;
- Layer completamente connessi (fully connected layer).

Uniamo questi livelli fino a formare un'architettura CNN completa.

##### Fully Connected Layer

Presa un'immagine di esempio $32\times32\times3$ che trasformata in un vettore a una dimensione diventa $3072\times1$.

![Fully Connected Layer](immagini/fully-connected-layer.png)

##### Convolution Layer

Presa un'immagine di esempio $32\times32\times3$ manteniamo la struttura spaziale, ma applichiamo un filtro che condensa l'informazione di un gruppo di pixel, questo implica che strati convoluzionali riducono la dimensione dell'immagine.

![Convolution Layer](immagini/convolution-layer.png)

Un esempio di filtro applicabile è lo smoothing gaussiano.
Per ogni pixel in un sottoinsieme dell'immagine originale si applica una funzione che calcola il valore per un pixel dell'immagine di output
$$
(f\cdot h)[n,m]=\sum_{k,l}f[k,l]\ h[n-k,m-l]
$$
Il singolo pixel viene calcolato come approssimazione di una funzione gaussiana bidimensionale:
$$
h(u,v)=\frac{1}{2\pi\sigma^2}e^{-\frac{u^2+v^2}{\sigma^2}}
$$

##### Pooling Layer

Un layer di pooling riduce lo spazio della rappresentazione per ridurre il numero di parametri e la complessità della computazione.
Questo tipo di layer lavora su ogni mappa di attivazione indipendentemente, dunque permette di eseguire downsampling su ogni strato dell'immagine indipendentemente.

![Pooling Layer](immagini/pooling-layer.png)

Un esempio tipico di pooling è applicare la funzione di massimizzazione per un gruppo di pixel.

![Max pooling layer](immagini/max-pooling-example.png)

#### Funzioni di attivazione

Esistono numerose funzioni di attivazione che possono essere utilizzate per ciascun neurone della rete.
In generale la proprietà maggiormente desiderabile è che sia derivabile, ma non è sempre necessario.

![Funzioni di attivazione](immagini/funzioni-di-attivazione.png)
